#!/usr/bin/env bash

######## Custom Methods ################
process_command()
{
  if [ $? -eq 0 ]
  then
    echo $1
  else
    echo $2
    exit 1
  fi
}
liferay-marketplace
echo "[$(date -u +%H:%M:%S)] ============== Start Building LPKG file ============== "

cd ..
echo "[$(date -u +%H:%M:%S)] Running Gradle task."
./gradlew build deploy

cd deploy || exit
echo "[$(date -u +%H:%M:%S)] Cleaning deploy directory."
rm -Rf *

echo "[$(date -u +%H:%M:%S)] Copying assembled JARs."
cp ../bundles/osgi/modules/com.aimprosoft.poc.*.jar .

echo "[$(date -u +%H:%M:%S)] Copying liferay-marketplace.properties."
cp ../_scripts/liferay-marketplace.properties .

echo "[$(date -u +%H:%M:%S)] Building archive for LPKG."
zip Liferay\ Request\ PoC.zip *

echo "[$(date -u +%H:%M:%S)] Building LPKG file."
mv Liferay\ Request\ PoC.zip Liferay\ Request\ PoC.lpkg

echo "[$(date -u +%H:%M:%S)] Cleaning files."
rm -Rf *.jar
rm -Rf .zip
rm -Rf *.zip
rm -Rf *.properties

echo "[$(date -u +%H:%M:%S)] ============== LPKG file bult succesfully ============== "