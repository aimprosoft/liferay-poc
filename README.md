# Liferay PoC Development 

## Initial Requirements

Technical Test

Create a liferay portlet that contain all information details based on result return from this soap webservice (http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?wsdl) using the operation name ‘FullCountryInfoAllCountries’

a) The portlet just need to display the details return from the webservice

b) The UI/theme/searching/etc., will based on your creativity (even just a simple table will also be okay as long as the result is displayed)

c) Also, the result return from this webservice need to be saved inside liferay database

The candidate need to document step-by-step on how they create the portlet and the result display.
At the end of it, please attach the compile jar/lar/code file, database/tables export file and the document on your email.

Pre-requisite software:-

Liferay DXP 7.1 and above (Please mention the version you use in the document)
Database : Any as long not Hypersonic (Please mention the DB you use in the document)
    
    
## Development Process 

### Prerequisites 

**Liferay version:** 7.3

**Database:** MySQL

**Instruments/tools:**

- IntelliJ IDEA;
- Liferay IntelliJ Plugin;
- Liferay Gradle Workspace;
- Liferay Target Platform.

### Coding Standards and Conventions

Package names prefix: `com.aimprosoft.poc.`

Bundle name: `com.aimprosoft.poc.`

Bundle symbolic name prefix: `Aim POC `

Module name prefix: `aim-poc-<module-name>`

### 1 - Define Architecture 

Modules and components diagram:

![images/01-modules-architecture.png](images/01-modules-architecture.png)

- **aim-poc-ws-client** - module for WebService communication (fetching countries from the web service);
- **aim-poc-sb** - Service Builder modules for database communication;
- **aim-poc-country-services** - custom API and Service, which encapsulates communication with WebService Client and Service Builder;
- **aim-poc-models** - shared module containing DTO models;
- **aim-poc-configuration** - module, which contains custom OSGi configuration for application;
- **aim-poc-sync-cronjob** - module with scheduled task for synchronization countries in DB with remote WebService countries;
- **aim-poc-countries-setup** - module with custom UpgradeProcess for importing countries into DB from the WebService;
- **aim-poc-countries-portlet** - module with custom portlet displaying countries.

### 2 - Environment Setup

2.1. Created new Liferay project as Liferay Gradle Workspace with Liferay IntelliJ Plugin.

2.2. Enabled the Target Platform

gradle.properties:

    liferay.workspace.target.platform.version = 7.3.0
    liferay.workspace.bundle.url=https://releases.liferay.com/portal/7.3.0-ga1/liferay-ce-portal-tomcat-7.3.0-ga1-20200127150653953.tar.gz
    target.platform.index.sources = false

2.3. Init the Liferay bundle:

![images/02-init-bundle.png](images/02-init-bundle.png)

2.4. Configured the Liferay Server:

![images/03-server-config.png](images/03-server-config.png)

2.5. Created MySQL database and configure Liferay connection:

portal-setup-wizard.properties:

    jdbc.default.driverClassName=com.mysql.cj.jdbc.Driver
    jdbc.default.url=jdbc:mysql://localhost:3306/liferay_request_poc?serverTimezone=Europe/Istanbul&useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
    jdbc.default.username=root
    jdbc.default.password=1
    
### 3 - Development

## 3.1. Web Service Client Implementation

- Created `aim-poc-ws-client` module
- Configured Gradle dependencies, required for Apache Axis:

        dependencies {
            compile group: 'axis', name: 'axis', version: '1.2'
            compile group: 'axis', name: 'axis-wsdl4j', version: '1.2'
            compile group: 'commons-logging', name: 'commons-logging', version: '1.2'
            compile group: 'commons-discovery', name: 'commons-discovery', version: '0.5'
            compile group: 'javax.xml', name: 'jaxrpc-api', version: '1.1'
            compile group: 'javax.xml.soap', name: 'javax.xml.soap-api', version: '1.4.0'
        }
    
- Generated Java code from WSDL with IDEA tools:

![images/04-wsdl-to-java.png](images/04-wsdl-to-java.png)

- Generated code:

![images/05-ws-code.png](images/05-ws-code.png)

## 3.2. Service Builder Implementation

- Generated service-builder module 

- Analyzed Java classes diagram from the required WebService:

![images/06-ws-models-disagram.png](images/06-ws-models-disagram.png)

- Designed appropriate database diagram from storing countries information:

![images/07-db-schema.png](images/07-db-schema.png)

- Defined entities in service.xml:

![images/08-sb-service.xml.png](images/08-sb-service.xml.png)

(defined entities and fields, custom finders and many-to-many relationship)

- Generated code with `buildService` command;

- Added custom methods to service and model classes:

    - com.aimprosoft.poc.sb.service.impl.TCountryLocalServiceImpl
    - com.aimprosoft.poc.sb.service.impl.TLanguageLocalServiceImpl
    - com.aimprosoft.poc.sb.model.impl.TCountryImpl
    
- Increased column length for 'flag' column in model-hints:

![images/09-model-hints.png](images/09-model-hints.png)

- Re-generated code with `buildService` command.

## 3.3. Models and Services Implementation

- Designed and implemented DTO model classes (`aim-poc-models` module):

![images/10-dto.png](images/10-dto.png)

- Designed API and created api module (`aim-poc-country-api` module):

![images/11-country-api.png](images/11-country-api.png)

- Implemented tha API in a service module (`aim-poc-country-service`):

[CountryService.java](https://bitbucket.org/aimprosoft/liferay-poc/src/master/modules/aim-poc-services/aim-poc-country-services/aim-poc-country-service/src/main/java/com/aimprosoft/poc/country/service/CountryService.java)

## 3.4. aim-poc-countries-setup module for Loading Countries Implementation

- Created `aim-poc-countries-setup` module;
- Implemented custom UpgradeStepRegistrator:
[CountriesSetupRegistrator.java](https://bitbucket.org/aimprosoft/liferay-poc/src/master/modules/aim-poc-services/aim-poc-countries-setup/src/main/java/com/aimprosoft/poc/countries/setup/CountriesSetupRegistrator.java)
- Implemented upgrade step for synchronization database data with Web Service:
[SetupCountriesUpgradeStep.java](https://bitbucket.org/aimprosoft/liferay-poc/src/master/modules/aim-poc-services/aim-poc-countries-setup/src/main/java/com/aimprosoft/poc/countries/setup/step/SetupCountriesUpgradeStep.java)

 ![images/12-upgrade-step.png](images/12-upgrade-step.png)

## 3.5. Portlet Implementation

- Created an `MVCPortlet` module;

- Defined the portlet class:

![images/13-portlet-class.png](images/13-portlet-class.png)

- Implemented a MVCRenderCommand class:
[ViewMVCRenderCommand.java](https://bitbucket.org/aimprosoft/liferay-poc/src/master/modules/aim-poc-portlets/aim-poc-countries-portlet/src/main/java/com/aimprosoft/poc/countries/portlet/action/ViewMVCRenderCommand.java)

- Implemented Search Container for articles displaying:

![images/14-sc.png](images/14-sc.png)

Result:

![images/15-countries.png](images/15-countries.png)

- Implemented country details page:

[country_details.jsp](https://bitbucket.org/aimprosoft/liferay-poc/src/master/modules/aim-poc-portlets/aim-poc-countries-portlet/src/main/resources/META-INF/resources/country_details.jsp)

- Implemented Friendly URLs for countries:

CountryFriendlyURLMapper:

![images/16-fum-java.png](images/16-fum-java.png)

routes.xml:

![images/17-fum-xml.png](images/17-fum-xml.png)

Sample country details page:

![images/18-country-details.png](images/18-country-details.png)

- Implemented mechanism for articles deletion and synchronization with WebService:

![images/19-delete-sync.png](images/19-delete-sync.png)
 
## 3.6. Configuration Implementation
 
- Created module `aim-poc-configuration` and implemented custom OSGi settings for scheduled task configuration.

Module: [aim-poc-configuration](https://bitbucket.org/aimprosoft/liferay-poc/src/master/modules/aim-poc-common/aim-poc-configuration/) 
 
Custom Configuration Category:

![images/20-osgi-settings.png](images/20-osgi-settings.png)

Countries Sync Configuration:

![images/21-cron-config.png](images/21-cron-config.png)

## 3.7. Cronjob implementation

- Created module `aim-poc-sync-cronjob` for scheduled task for countries synchronization.

- Implemented custom [cron job](https://bitbucket.org/aimprosoft/liferay-poc/src/master/modules/aim-poc-services/aim-poc-sync-cronjob/src/main/java/com/aimprosoft/poc/sync/cronjob/CountriesSyncCronJob.java) 
as `BaseMessageListener` implementation;

- Used cronjob configuration from `aim-poc-configuration` module;

- Invoked `_countryAPI.sync()` for synchronization database countries with WebService. 

### 4 - Deployment/delivery

### Demo Server

Demo server URL: [http://liferay-demo.aimprosoft.com/](http://liferay-demo.aimprosoft.com/)

Credentials: test@liferay.com / test

Page with Countries portlet: [http://liferay-demo.aimprosoft.com/web/liferay-poc/countries](http://liferay-demo.aimprosoft.com/web/liferay-poc/countries)

Settings: [Countries Sync Configuration](http://liferay-demo.aimprosoft.com/group/control_panel/manage?p_p_id=com_liferay_configuration_admin_web_portlet_SystemSettingsPortlet&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&_com_liferay_configuration_admin_web_portlet_SystemSettingsPortlet_factoryPid=com.aimprosoft.poc.osgi.configuration.CountriesSyncConfiguration&_com_liferay_configuration_admin_web_portlet_SystemSettingsPortlet_mvcRenderCommandName=%2Fedit_configuration&_com_liferay_configuration_admin_web_portlet_SystemSettingsPortlet_pid=com.aimprosoft.poc.osgi.configuration.CountriesSyncConfiguration)

### Local Deployment Instructions

- Deploy [Liferay Request PoC.lpkg](https://bitbucket.org/aimprosoft/liferay-poc/src/master/deploy/) file;

`Check bundles:`

![images/22-bundles.png](images/22-bundles.png)

- Add 'Countries' portlet to a page:

![images/23-add-portlet.png](images/23-add-portlet.png)

- Enjoy :

![images/24-enjoy.png](images/24-enjoy.png)