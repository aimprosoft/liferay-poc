<%@ include file="/init.jsp" %>
<%
    CountryAPI countryAPI = (CountryAPI)request.getAttribute("countryAPI");
    String baseUrl = (String)request.getAttribute("baseUrl");
%>

<portlet:actionURL var="syncUrl" name="/countries/sync">
</portlet:actionURL>

<liferay-ui:success key="sync-message" message="${syncMessage}"  />

<div class="container-fluid container-fluid-max-xl container-form-lg">
    <div class="row">
        <div class="col-md-12">
            <h1>Countries List</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <liferay-ui:icon
                    cssClass="sync-icon"
                    icon="reload"
                    label="<%= false %>"
                    linkCssClass="control-menu-icon"
                    markupView="lexicon"
                    message="Sync"
                    url="${syncUrl}"
            />
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <liferay-ui:search-container
                    emptyResultsMessage="no-countries-found"
                    total="${countryAPI.fetchCountriesCount()}"
                    delta="10">
                <liferay-ui:search-container-results results="${countryAPI.fetchCountries(searchContainer.getStart(), searchContainer.getEnd())}" />
                <liferay-ui:search-container-row className="com.aimprosoft.poc.models.Country" modelVar="country">
                    <liferay-ui:search-container-column-text name="#">
                        <a href="${baseUrl}/-/country/${country.code}">
                                ${country.countryId}
                        </a>
                    </liferay-ui:search-container-column-text>
                    <liferay-ui:search-container-column-text name="country.code" value="${country.code}" />
                    <liferay-ui:search-container-column-text name="country.name" value="${country.name}" />
                    <liferay-ui:search-container-column-text name="country.capital" value="${country.capital}" />
                    <liferay-ui:search-container-column-text name="country.phone" value="${country.phoneCode}" />
                    <liferay-ui:search-container-column-text name="country.continent" value="${country.continentCode}" />
                    <liferay-ui:search-container-column-text name="country.currency" value="${country.currency}" />
                    <liferay-ui:search-container-column-image name="country.flag" src="${country.flag}"  />
                    <liferay-ui:search-container-column-text name="country.actions">
                        <liferay-ui:icon-menu markupView="lexicon">
                            <portlet:actionURL var="deleteUrl" name="/countries/delete">
                                <portlet:param name="countryId" value="${country.countryId}"/>
                            </portlet:actionURL>
                            <liferay-ui:icon
                                    message="country.action.details"
                                    url="${baseUrl}/-/country/${country.code}"
                            />
                            <liferay-ui:icon-delete message="country.action.delete" url="${deleteUrl}"
                                                    cssClass="action-icon trash-icon"
                                                    confirmation="country.action.delete.confirmation"
                            />
                        </liferay-ui:icon-menu>
                    </liferay-ui:search-container-column-text>
                </liferay-ui:search-container-row>
                <liferay-ui:search-iterator markupView="lexicon" />
            </liferay-ui:search-container>
        </div>
    </div>
</div>



