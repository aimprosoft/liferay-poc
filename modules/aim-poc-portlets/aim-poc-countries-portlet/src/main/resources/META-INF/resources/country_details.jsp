<%@ include file="/init.jsp" %>

<div class="container-fluid container-fluid-max-xl container-form-lg">
    <div class="row">
        <div class="col-lg-12">
            <aui:form class="form">
                <div class="sheet sheet-lg">
                    <div class="sheet-header">
                        <h2 class="sheet-title">${country.name}</h2>
                    </div>
                    <div class="sheet-section">
                        <div class="form-group">
                            <h3 class="sheet-subtitle">
                                <liferay-ui:message key="country.information" />
                            </h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <aui:input name="countryId" value="${country.countryId}" disabled="true" label="country.id" />
                                    <aui:input name="countryCode" value="${country.code}" label="country.code" />
                                    <aui:input name="countryName" value="${country.name}" label="country.name" />
                                    <aui:input name="capital" value="${country.capital}" label="country.capital" />
                                    <aui:input name="phone" value="${country.phoneCode}" label="country.phone" />
                                    <aui:input name="continent" value="${country.continentCode}" label="country.continent" />
                                    <aui:input name="currency" value="${country.currency}" label="country.currency" />
                                </div>
                                <div class="col-md-6 text-center">
                                    <img src="${country.flag}" alt="flag" width="200" height="200" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <h3 class="sheet-subtitle"><liferay-ui:message key="country.languages" /></h3>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-autofit table-heading-nowrap table-list">
                                            <thead>
                                                <tr>
                                                    <th class="table-cell-expand table-cell-minw-150">
                                                        <liferay-ui:message key="country.language.id" />
                                                    </th>
                                                    <th class="table-cell-expand table-cell-minw-200">
                                                        <liferay-ui:message key="country.language.code" />
                                                    </th>
                                                    <th class="table-cell-expand table-cell-minw-200">
                                                        <liferay-ui:message key="country.language.name" />
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="language" items="${country.languages}">
                                                    <tr>
                                                        <td class="table-cell-expand-small table-cell-minw-150">
                                                            ${language.languageId}
                                                        </td>
                                                        <td class="table-cell-expand table-cell-minw-200">
                                                            ${language.code}
                                                        </td>
                                                        <td class="table-cell-expand table-cell-minw-200">
                                                            ${language.name}
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sheet-footer">
                        <a href="${baseUrl}" class="btn btn-primary">
                            <liferay-ui:message key="btn.back" />
                        </a>
                    </div>
                </div>
            </aui:form>
        </div>
    </div>
</div>