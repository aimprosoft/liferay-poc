package com.aimprosoft.poc.countries.portlet.action;

import com.aimprosoft.poc.countries.constants.CountriesPortletKeys;
import com.aimprosoft.poc.country.api.CountryAPI;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + CountriesPortletKeys.PORTLET_ID,
                "mvc.command.name=/countries/delete"
        },
        service = MVCActionCommand.class
)
public class DeleteCountryMVCActionCommand extends BaseMVCActionCommand {


    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        try {
            long countryId = ParamUtil.getLong(actionRequest, "countryId");
            _countryAPI.deleteCountry(countryId);
        } catch (Exception e) {
            _log.error("Failed to delete country, cause: " + e.getMessage());
        }
    }

    @Reference
    private CountryAPI _countryAPI;

    private static final Log _log = LogFactoryUtil.getLog(DeleteCountryMVCActionCommand.class.getName());
}
