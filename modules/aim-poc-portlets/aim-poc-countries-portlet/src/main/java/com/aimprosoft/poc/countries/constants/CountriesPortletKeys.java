package com.aimprosoft.poc.countries.constants;

public class CountriesPortletKeys {

	public static final String CATEGORY_NAME = "Aimprosoft";

	public static final String DISPLAY_NAME = "Countries Portlet";

	public static final String PORTLET_ID = "com_aimprosoft_poc_countries_portlet_CountriesPortlet";

	public static final String CSS_CLASS_WRAPPER = "countries-portlet-wrapper";


	public static final String VIEW_JSP = "/view.jsp";
	public static final String DETAILS_JSP = "/country_details.jsp";
}