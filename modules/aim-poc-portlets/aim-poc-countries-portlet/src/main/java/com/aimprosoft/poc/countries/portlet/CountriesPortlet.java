package com.aimprosoft.poc.countries.portlet;

import com.aimprosoft.poc.countries.constants.CountriesPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=" + CountriesPortletKeys.CATEGORY_NAME,
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=" + CountriesPortletKeys.DISPLAY_NAME,
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=" + CountriesPortletKeys.VIEW_JSP,
		"javax.portlet.name=" + CountriesPortletKeys.PORTLET_ID,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.supported-public-render-parameter=countryCode",
		"com.liferay.portlet.css-class-wrapper=" + CountriesPortletKeys.CSS_CLASS_WRAPPER
	},
	service = Portlet.class
)
public class CountriesPortlet extends MVCPortlet {

}