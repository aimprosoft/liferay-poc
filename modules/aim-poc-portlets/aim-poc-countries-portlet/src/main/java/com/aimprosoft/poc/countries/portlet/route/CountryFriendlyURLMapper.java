package com.aimprosoft.poc.countries.portlet.route;

import com.aimprosoft.poc.countries.constants.CountriesPortletKeys;
import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;
import com.liferay.portal.kernel.portlet.FriendlyURLMapper;
import org.osgi.service.component.annotations.Component;

@Component(
        property = {
            "com.liferay.portlet.friendly-url-routes=META-INF/friendly-url-routes/routes.xml",
            "javax.portlet.name=" + CountriesPortletKeys.PORTLET_ID
        },
        service = FriendlyURLMapper.class
)
public class CountryFriendlyURLMapper extends DefaultFriendlyURLMapper {

    @Override
    public String getMapping() {
        return _MAPPING;
    }

    private static final String _MAPPING = "country";

}
