package com.aimprosoft.poc.countries.portlet.action;

import com.aimprosoft.poc.countries.constants.CountriesPortletKeys;
import com.aimprosoft.poc.country.api.CountryAPI;
import com.aimprosoft.poc.models.Country;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + CountriesPortletKeys.PORTLET_ID,
                "mvc.command.name=/",
                "mvc.command.name=/countries/view"
        },
        service = MVCRenderCommand.class
)
public class ViewMVCRenderCommand implements MVCRenderCommand {

    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
        String countryCode = ParamUtil.getString(renderRequest, "countryCode");
        String baseUrl = getBaseUrl(renderRequest);
        renderRequest.setAttribute("baseUrl", baseUrl);
        if (Validator.isBlank(countryCode)) {
            renderRequest.setAttribute("countryAPI", _countryAPI);
            return CountriesPortletKeys.VIEW_JSP;
        } else {
            Country country = _countryAPI.fetchCountry(countryCode);
            renderRequest.setAttribute("country", country);
            return CountriesPortletKeys.DETAILS_JSP;
        }
    }

    private String getBaseUrl(RenderRequest renderRequest) {
        HttpServletRequest httpServletRequest = _portal.getHttpServletRequest(renderRequest);
        String currentURL = _portal.getCurrentURL(httpServletRequest);
        if (currentURL.contains(StringPool.QUESTION)) {
            return StringUtil.split(currentURL, StringPool.QUESTION)[0];
        }
        if (currentURL.contains(Portal.FRIENDLY_URL_SEPARATOR)) {
            return StringUtil.split(currentURL, Portal.FRIENDLY_URL_SEPARATOR)[0];
        }
        return currentURL;
    }

    @Reference
    private Portal _portal;
    @Reference
    private CountryAPI _countryAPI;
}
