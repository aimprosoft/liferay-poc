package com.aimprosoft.poc.countries.portlet.action;

import com.aimprosoft.poc.countries.constants.CountriesPortletKeys;
import com.aimprosoft.poc.country.api.CountryAPI;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Portal;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.servlet.http.HttpServletRequest;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + CountriesPortletKeys.PORTLET_ID,
                "mvc.command.name=/countries/sync"
        },
        service = MVCActionCommand.class
)
public class SyncCountriesMVCActionCommand extends BaseMVCActionCommand {

    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        try {
            int syncedCountries = _countryAPI.sync();

            HttpServletRequest request = _portal.getHttpServletRequest(actionRequest);

            String syncMessage = syncedCountries > 0 ?
                    LanguageUtil.format(request, "country.sync.x.synced", String.valueOf(syncedCountries)) :
                    LanguageUtil.get(request, "country.sync.up.to.date");

            actionRequest.setAttribute("syncMessage", syncMessage);
            SessionMessages.add(actionRequest, "sync-message", syncMessage);
        } catch (Exception e) {
            _log.error("Failed to delete country, cause: " + e.getMessage());
        }
    }


    @Reference
    private Portal _portal;
    @Reference
    private CountryAPI _countryAPI;

    private static final Log _log = LogFactoryUtil.getLog(SyncCountriesMVCActionCommand.class.getName());
}
