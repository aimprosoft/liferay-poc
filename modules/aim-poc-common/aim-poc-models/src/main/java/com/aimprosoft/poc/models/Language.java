package com.aimprosoft.poc.models;

import java.io.Serializable;

public class Language implements Serializable {

    private long languageId;
    private long companyId;
    private String code;
    private String name;

    public Language() {
    }

    public Language(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public Language(long languageId, long companyId, String code, String name) {
        this.languageId = languageId;
        this.companyId = companyId;
        this.code = code;
        this.name = name;
    }

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
