package com.aimprosoft.poc.models.exception;

public class CountryServiceException extends Exception {

    public CountryServiceException() {
        super();
    }

    public CountryServiceException(String message) {
        super(message);
    }

    public CountryServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public CountryServiceException(Throwable cause) {
        super(cause);
    }
}
