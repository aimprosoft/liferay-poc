package com.aimprosoft.poc.osgi.configuration;

import aQute.bnd.annotation.metatype.Meta;
import com.aimprosoft.poc.osgi.AimPocConfigurationKeys;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

@ExtendedObjectClassDefinition(
        category = AimPocConfigurationKeys.CATEGORY_KEY,
        scope = ExtendedObjectClassDefinition.Scope.SYSTEM
)
@Meta.OCD(
        id = AimPocConfigurationKeys.COUNTRIES_SYNC_CONFIGURATION_ID,
        localization = AimPocConfigurationKeys.RESOURCE_BUNDLE,
        name = AimPocConfigurationKeys.COUNTRIES_SYNC_CONFIGURATION_NAME,
        description = AimPocConfigurationKeys.COUNTRIES_SYNC_CONFIGURATION_DESC
)
public interface CountriesSyncConfiguration {

    @Meta.AD(
            name = "countries-sync-enabled",
            description = "countries-sync-enabled-desc",
            required = false,
            deflt = "false"
    )
    public boolean syncEnabled();

    @Meta.AD(
            name = "countries-sync-cron-expression",
            description = "countries-sync-cron-expression-desc",
            required = false,
            deflt = "0 * * * * ?"
    )
    public String cronExpression();



}
