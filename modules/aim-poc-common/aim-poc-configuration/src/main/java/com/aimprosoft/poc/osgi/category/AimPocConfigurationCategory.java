package com.aimprosoft.poc.osgi.category;

import com.aimprosoft.poc.osgi.AimPocConfigurationKeys;
import com.liferay.configuration.admin.category.ConfigurationCategory;
import org.osgi.service.component.annotations.Component;

@Component(service = ConfigurationCategory.class)
public class AimPocConfigurationCategory implements ConfigurationCategory {

    @Override
    public String getCategoryKey() {
        return AimPocConfigurationKeys.CATEGORY_KEY;
    }

    @Override
    public String getCategorySection() {
        return AimPocConfigurationKeys.CATEGORY_SECTION;
    }

    @Override
    public String getCategoryIcon() {
        return AimPocConfigurationKeys.CATEGORY_ICON;
    }

}
