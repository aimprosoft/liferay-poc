package com.aimprosoft.poc.osgi;

public interface AimPocConfigurationKeys {

    String CATEGORY_ICON = "cog";

    String CATEGORY_SECTION = "other";

    String CATEGORY_KEY = "aim-poc-settings";

    String COUNTRIES_SYNC_CONFIGURATION_ID = "com.aimprosoft.poc.osgi.configuration.CountriesSyncConfiguration";
    String COUNTRIES_SYNC_CONFIGURATION_NAME = "countries-sync-configuration";
    String COUNTRIES_SYNC_CONFIGURATION_DESC = "countries-sync-configuration-desc";

    String RESOURCE_BUNDLE = "content/Language";
}
