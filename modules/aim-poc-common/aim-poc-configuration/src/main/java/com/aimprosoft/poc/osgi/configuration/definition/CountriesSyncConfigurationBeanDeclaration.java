package com.aimprosoft.poc.osgi.configuration.definition;

import com.aimprosoft.poc.osgi.configuration.CountriesSyncConfiguration;
import com.liferay.portal.kernel.settings.definition.ConfigurationBeanDeclaration;
import org.osgi.service.component.annotations.Component;

@Component
public class CountriesSyncConfigurationBeanDeclaration implements ConfigurationBeanDeclaration {

    @Override
    public Class<?> getConfigurationBeanClass() {
        return CountriesSyncConfiguration.class;
    }

}
