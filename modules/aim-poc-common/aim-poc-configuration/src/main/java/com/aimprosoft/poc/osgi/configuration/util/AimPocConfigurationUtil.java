package com.aimprosoft.poc.osgi.configuration.util;

import com.aimprosoft.poc.osgi.configuration.CountriesSyncConfiguration;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.module.configuration.ConfigurationException;
import com.liferay.portal.kernel.module.configuration.ConfigurationProviderUtil;

public class AimPocConfigurationUtil {

    public static CountriesSyncConfiguration getCountriesSyncConfiguration(){
        try {
            return ConfigurationProviderUtil.getSystemConfiguration(CountriesSyncConfiguration.class);
        }
        catch (ConfigurationException ce) {
            _log.error("Unable to load CountriesSyncConfiguration", ce);
        }
        return null;
    }

    private static final Log _log = LogFactoryUtil.getLog(AimPocConfigurationUtil.class);
}
