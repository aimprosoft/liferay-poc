package com.aimprosoft.poc.country.service;

import com.aimprosoft.poc.country.api.CountryAPI;
import com.aimprosoft.poc.models.Country;
import com.aimprosoft.poc.models.Language;
import com.aimprosoft.poc.models.exception.CountryServiceException;
import com.aimprosoft.poc.sb.model.TCountry;
import com.aimprosoft.poc.sb.service.TCountryLocalService;
import com.aimprosoft.poc.ws.client.ArrayOftCountryInfo;
import com.aimprosoft.poc.ws.client.CountryInfoServiceLocator;
import com.aimprosoft.poc.ws.client.CountryInfoServiceSoapType;
import com.aimprosoft.poc.ws.client.TCountryInfo;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.Portal;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component(
	immediate = true,
	property = {
	},
	service = CountryAPI.class
)
public class CountryService extends BaseCountryService implements CountryAPI {

	@Override
	public int sync() throws CountryServiceException {
		try {
			List<Country> countries = fetchRemoteCountries();
			int savedCountries = 0;
			for (Country country : countries) {
				if (!countryExists(country.getCode())) {
					Country savedCountry = saveCountry(country);
					if (_log.isInfoEnabled()) {
						long countryId = savedCountry.getCountryId();
						String countryName = savedCountry.getName();
						_log.info(String.format("Saved country #%d \"%s\"", countryId, countryName));
					}
					savedCountries++;
				}
			}
			return savedCountries;
		} catch (Exception e) {
			String errorMsg = "Failed to sync countries with  web service, cause: " + e.getMessage();
			_log.error(errorMsg);
			throw new CountryServiceException(errorMsg, e);
		}
	}

	@Override
	public List<Country> fetchRemoteCountries() throws CountryServiceException {
		List<Country> countries = new ArrayList<>();
		try {

			CountryInfoServiceLocator serviceLocator = new CountryInfoServiceLocator();
			CountryInfoServiceSoapType countryInfoServiceSoap = serviceLocator.getCountryInfoServiceSoap();
			ArrayOftCountryInfo arrayOftCountryInfo = countryInfoServiceSoap.fullCountryInfoAllCountries();

			if (arrayOftCountryInfo == null) {
				throw new CountryServiceException("Received null ArrayOftCountryInfo object from Country Web Service.");
			}

			TCountryInfo[] tCountryInfo = arrayOftCountryInfo.getTCountryInfo();
			if (tCountryInfo == null || tCountryInfo.length == 0) {
				throw new CountryServiceException("Received empty of null TCountryInfo array from Country Web Service.");
			}

			for (TCountryInfo countryInfo : tCountryInfo) {
				Country country = toCountry(countryInfo);
				countries.add(country);
			}

		} catch (Exception e) {
			String errorMsg = "Failed to fetch countries list from web service, cause: " + e.getMessage();
			_log.error(errorMsg);
			throw new CountryServiceException(errorMsg, e);
		}
		return countries;
	}

	@Override
	public List<Country> fetchCountries() {
		List<TCountry> tCountries = _tCountryLocalService.getTCountries(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		return toCountries(tCountries);
	}

	@Override
	public int fetchCountriesCount() {
		return _tCountryLocalService.getTCountriesCount();
	}

	@Override
	public List<Country> fetchCountries(int start, int end) {
		List<TCountry> tCountries = _tCountryLocalService.getTCountries(start, end);
		return toCountries(tCountries);
	}

	@Override
	public boolean countryExists(String countryCode) {
		return _tCountryLocalService.countryExists(countryCode);
	}

	@Override
	public Country fetchCountry(String countryCode) {
		TCountry country = _tCountryLocalService.getCountryByCode(countryCode);
		return toCountry(country);
	}

	@Override
	public Country saveCountry(Country country) {

		long companyId = country.getCompanyId();
		if (companyId <= 0) {
			companyId = _portal.getDefaultCompanyId();
		}

		Map<String, String> languageMap = new HashMap<>();
		List<Language> languages = country.getLanguages();
		if (ListUtil.isNotEmpty(languages)) {
			for (Language language : languages) {
				String languageCode = language.getCode();
				String languageName = language.getName();
				languageMap.put(languageCode, languageName);
			}
		}

		TCountry savedCountry = _tCountryLocalService.addTCountry(
				companyId,
				country.getCode(),
				country.getName(),
				country.getCapital(),
				country.getPhoneCode(),
				country.getContinentCode(),
				country.getCurrency(),
				country.getFlag(),
				languageMap
		);

		return toCountry(savedCountry);
	}


	@Override
	public void deleteCountry(long countryId) throws CountryServiceException {
		try {
			_tCountryLocalService.deleteTCountry(countryId);
		} catch (Exception e) {
			throw new CountryServiceException("Can not delete country. cause: " + e.getMessage(), e);
		}
	}

	@Reference
	private Portal _portal;
	@Reference
	private TCountryLocalService _tCountryLocalService;

	private static final Log _log = LogFactoryUtil.getLog(CountryService.class.getName());
}