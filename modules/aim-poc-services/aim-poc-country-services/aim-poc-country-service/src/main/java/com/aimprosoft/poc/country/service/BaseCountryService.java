package com.aimprosoft.poc.country.service;

import com.aimprosoft.poc.models.Country;
import com.aimprosoft.poc.models.Language;
import com.aimprosoft.poc.sb.model.TCountry;
import com.aimprosoft.poc.ws.client.ArrayOftLanguage;
import com.aimprosoft.poc.ws.client.TCountryInfo;
import com.aimprosoft.poc.ws.client.TLanguage;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.ListUtil;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseCountryService {

    protected Country toCountry(TCountryInfo countryInfo) {
        Country country = new Country();

        country.setCode(countryInfo.getSISOCode());
        country.setName(countryInfo.getSName());
        country.setCapital(countryInfo.getSCapitalCity());
        country.setPhoneCode(countryInfo.getSPhoneCode());
        country.setContinentCode(countryInfo.getSContinentCode());
        country.setCurrency(countryInfo.getSCurrencyISOCode());
        country.setFlag(countryInfo.getSCountryFlag());

        List<Language> languages = getLanguages(countryInfo);
        country.setLanguages(languages);

        return country;
    }

    private List<Language> getLanguages(TCountryInfo countryInfo) {
        List<Language> languagesList = new ArrayList<>();
        ArrayOftLanguage languages = countryInfo.getLanguages();
        if (languages != null) {
            TLanguage[] tLanguages = languages.getTLanguage();
            if (ArrayUtil.isNotEmpty(tLanguages)) {
                for (TLanguage tLanguage : tLanguages) {
                    Language language = new Language(tLanguage.getSISOCode(), tLanguage.getSName());
                    languagesList.add(language);
                }
            }
        }
        return languagesList;
    }

    protected List<Country> toCountries(List<TCountry> tCountries){
        List<Country> countries = new ArrayList<>();
        if (ListUtil.isNotEmpty(tCountries)) {
            for (TCountry tCountry : tCountries) {
                Country country = toCountry(tCountry);
                countries.add(country);
            }
        }
        return countries;
    }

    protected Country toCountry(TCountry tCountry) {
        Country country = new Country();

        country.setCountryId(tCountry.getCountryId());
        country.setCompanyId(tCountry.getCompanyId());
        country.setCode(tCountry.getCode());
        country.setName(tCountry.getName());
        country.setCapital(tCountry.getCapital());
        country.setPhoneCode(tCountry.getPhoneCode());
        country.setContinentCode(tCountry.getContinentCode());
        country.setCurrency(tCountry.getCurrency());
        country.setFlag(tCountry.getFlag());

        List<Language> languages = getLanguages(tCountry);
        country.setLanguages(languages);

        return country;
    }

    private List<Language> getLanguages(TCountry tCountry) {
        List<Language> languagesList = new ArrayList<>();
        List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages = tCountry.getLanguages();
        if (ListUtil.isNotEmpty(tLanguages)) {
            for (com.aimprosoft.poc.sb.model.TLanguage tLanguage : tLanguages) {
                Language language = new Language(tLanguage.getLanguageId(), tLanguage.getCompanyId(),
                        tLanguage.getCode(), tLanguage.getName());
                languagesList.add(language);
            }
        }
        return languagesList;
    }

}
