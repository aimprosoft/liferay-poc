package com.aimprosoft.poc.country.api;

import com.aimprosoft.poc.models.Country;
import com.aimprosoft.poc.models.exception.CountryServiceException;

import java.util.List;

public interface CountryAPI {

    /**
     * Syncs DB country records with remote WebService data
     *
     * @throws CountryServiceException
     */
    int sync() throws CountryServiceException;

    /**
     * Fetches list of countries from remote WebService
     *
     * @return list of countries
     */
    List<Country> fetchRemoteCountries() throws CountryServiceException;

    /**
     * Fetches list of countries from Database
     *
     * @return list of countries
     */
    List<Country> fetchCountries();


    /**
     * Fetches countries count
     *
     * @return countries count
     */
    int fetchCountriesCount();

    /**
     * Fetches paginated list of countries from Database
     *
     * @param start - start position
     * @param end - end position
     *
     * @return list of countries
     */
    List<Country> fetchCountries(int start, int end);


    /**
     * Checks if country exists in the database
     *
     * @param countryCode Country code
     *
     * @return true, if country exists
     */
    boolean countryExists(String countryCode);


    /**
     * Fetches country by country code
     *
     * @param countryCode Country Code
     *
     * @return Country
     */
    Country fetchCountry(String countryCode);

    /**
     * Saves country to database
     *
     * @param country Country model
     *
     * @return saved country
     */
    Country saveCountry(Country country);

    /**
     * Deletes country
     *
     * @param countryId ID of country in Liferay deatabase
     */
    void deleteCountry(long countryId) throws CountryServiceException;

}