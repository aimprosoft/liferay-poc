package com.aimprosoft.poc.sync.cronjob;

import com.aimprosoft.poc.country.api.CountryAPI;
import com.aimprosoft.poc.osgi.AimPocConfigurationKeys;
import com.aimprosoft.poc.osgi.configuration.CountriesSyncConfiguration;
import com.aimprosoft.poc.osgi.configuration.util.AimPocConfigurationUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.scheduler.*;
import org.osgi.service.component.annotations.*;

import java.util.Date;
import java.util.Map;

@Component(
        configurationPid = AimPocConfigurationKeys.COUNTRIES_SYNC_CONFIGURATION_ID,
        immediate = true,
        property = {
        },
        service = BaseMessageListener.class
)
public class CountriesSyncCronJob extends BaseMessageListener {

    private static final String CRON_EXPRESSION_DEFAULT = "0 * * * * ?";
    private static final boolean CRON_ENABLED_DEFAULT = false;

    @Activate
    @Modified
    protected void activate(Map<String, Object> properties) throws SchedulerException {

        String cronExpresion = CRON_EXPRESSION_DEFAULT;
        boolean cronEnabled = CRON_ENABLED_DEFAULT;

        CountriesSyncConfiguration syncConfiguration = AimPocConfigurationUtil.getCountriesSyncConfiguration();
        if (syncConfiguration != null) {
            cronEnabled = syncConfiguration.syncEnabled();
            cronExpresion = syncConfiguration.cronExpression();
        }

        if (cronEnabled) {
            String listenerClass = getClass().getName();
            Trigger jobTrigger = _triggerFactory.createTrigger(listenerClass, listenerClass, new Date(), null, cronExpresion);

            _schedulerEntryImpl = new SchedulerEntryImpl(getClass().getName(), jobTrigger);

            if (_initialized) {
                deactivate();
            }

            _schedulerEngineHelper.register(this, _schedulerEntryImpl, DestinationNames.SCHEDULER_DISPATCH);
            _log.debug("Scheduled task registered: " + cronExpresion);

            _initialized = true;
        }

    }

    @Override
    protected void doReceive(Message message) throws Exception {
        try {
            if (_log.isInfoEnabled()) {
                _log.info("CountriesSyncCronJob: start countries sync.");
            }

            int processedCountries = _countryAPI.sync();

            if (_log.isInfoEnabled()) {
                _log.info("Added countries: " + processedCountries);
            }

        } catch (Exception e) {
            _log.error("Failed to sync countries, cause: " + e.getMessage());
        }
    }

    /**
     * deactivate: Called when OSGi is deactivating the component.
     */
    @Deactivate
    protected void deactivate() {
        // if we previously were initialized
        if (_initialized) {
            // un-schedule the job so it is cleaned up
            try {
                _schedulerEngineHelper.unschedule(_schedulerEntryImpl, getStorageType());
            } catch (SchedulerException se) {
                if (_log.isWarnEnabled()) {
                    _log.warn("Unable to unschedule trigger", se);
                }
            }

            // unregister this listener
            _schedulerEngineHelper.unregister(this);
        }

        // clear the initialized flag
        _initialized = false;
    }

    protected StorageType getStorageType() {
        if (_schedulerEntryImpl instanceof StorageTypeAware) {
            return ((StorageTypeAware) _schedulerEntryImpl).getStorageType();
        }
        return StorageType.MEMORY_CLUSTERED;
    }


    private volatile boolean _initialized;
    private SchedulerEntryImpl _schedulerEntryImpl = null;

    @Reference
    private TriggerFactory _triggerFactory;
    @Reference
    private SchedulerEngineHelper _schedulerEngineHelper;
    @Reference
    private CountryAPI _countryAPI;

    private static final Log _log = LogFactoryUtil.getLog(CountriesSyncCronJob.class);

}
