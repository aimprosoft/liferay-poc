/**
 * CountryInfoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2 May 03, 2005 (02:20:24 EDT) WSDL2Java emitter.
 */

package com.aimprosoft.poc.ws.client;

public interface CountryInfoService extends javax.xml.rpc.Service {

/**
 * This DataFlex Web Service opens up country information. 2 letter
 * ISO codes are used for Country code. There are functions to retrieve
 * the used Currency, Language, Capital City, Continent and Telephone
 * code.
 */
    public java.lang.String getCountryInfoServiceSoap12Address();

    public CountryInfoServiceSoapType getCountryInfoServiceSoap12() throws javax.xml.rpc.ServiceException;

    public CountryInfoServiceSoapType getCountryInfoServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getCountryInfoServiceSoapAddress();

    public CountryInfoServiceSoapType getCountryInfoServiceSoap() throws javax.xml.rpc.ServiceException;

    public CountryInfoServiceSoapType getCountryInfoServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
