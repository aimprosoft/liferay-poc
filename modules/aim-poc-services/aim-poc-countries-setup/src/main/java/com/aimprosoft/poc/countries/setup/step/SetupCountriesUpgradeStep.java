package com.aimprosoft.poc.countries.setup.step;

import com.aimprosoft.poc.country.api.CountryAPI;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class SetupCountriesUpgradeStep  extends UpgradeProcess {

    private final CountryAPI countryAPI;

    public SetupCountriesUpgradeStep(CountryAPI countryAPI) {
        this.countryAPI = countryAPI;
    }

    @Override
    protected void doUpgrade() throws Exception {
        try {
            countryAPI.sync(); //Sync DB records with WebService data
        } catch (Exception e) {
            _log.error("Failed to sync countries, cause: " + e.getMessage());
        }
    }

    private static final Log _log = LogFactoryUtil.getLog(SetupCountriesUpgradeStep.class.getName());
}
