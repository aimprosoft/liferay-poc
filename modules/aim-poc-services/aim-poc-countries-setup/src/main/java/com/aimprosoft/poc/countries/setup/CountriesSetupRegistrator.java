package com.aimprosoft.poc.countries.setup;

import com.aimprosoft.poc.countries.setup.step.SetupCountriesUpgradeStep;
import com.aimprosoft.poc.country.api.CountryAPI;
import com.liferay.portal.kernel.upgrade.DummyUpgradeStep;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(
        immediate = true,
        service = UpgradeStepRegistrator.class
)
public class CountriesSetupRegistrator implements UpgradeStepRegistrator {

    @Override
    public void register(Registry registry) {
        registry.register("0.0.0", "1.0.0", new DummyUpgradeStep());
        registry.register("1.0.0", "1.0.1", new SetupCountriesUpgradeStep(_countryAPI));
    }

    @Reference
    private CountryAPI _countryAPI;

}