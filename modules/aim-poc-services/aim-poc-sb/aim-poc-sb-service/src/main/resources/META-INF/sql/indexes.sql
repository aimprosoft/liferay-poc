create index IX_B5A8A4E0 on aim_TCountry (code_[$COLUMN_LENGTH:75$]);

create index IX_DC1218F8 on aim_TCountry_TLanguages (companyId);
create index IX_D57E0577 on aim_TCountry_TLanguages (languageId);

create index IX_ADAF52AA on aim_TLanguage (code_[$COLUMN_LENGTH:75$]);