create table aim_TCountry (
	countryId LONG not null primary key,
	companyId LONG,
	code_ VARCHAR(75) null,
	name VARCHAR(75) null,
	capital VARCHAR(75) null,
	phoneCode VARCHAR(75) null,
	continentCode VARCHAR(75) null,
	currency_ VARCHAR(75) null,
	flag VARCHAR(255) null
);

create table aim_TCountry_TLanguages (
	companyId LONG not null,
	countryId LONG not null,
	languageId LONG not null,
	primary key (countryId, languageId)
);

create table aim_TLanguage (
	languageId LONG not null primary key,
	companyId LONG,
	code_ VARCHAR(75) null,
	name VARCHAR(75) null
);