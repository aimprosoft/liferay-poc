/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.model.impl;

import com.aimprosoft.poc.sb.model.TLanguage;
import com.aimprosoft.poc.sb.service.TLanguageLocalServiceUtil;
import org.osgi.annotation.versioning.ProviderType;

import java.util.Collections;
import java.util.List;

/**
 * The extended model implementation for the TCountry service. Represents a row in the &quot;aim_TCountry&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.aimprosoft.poc.sb.model.TCountry</code> interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
@ProviderType
public class TCountryImpl extends TCountryBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a t country model instance should use the {@link com.aimprosoft.poc.sb.model.TCountry} interface instead.
	 */
	public TCountryImpl() {
	}

	public List<TLanguage> getLanguages(){

		long countryId = getCountryId();
		if (countryId <= 0) {
			return Collections.emptyList();
		}

		return TLanguageLocalServiceUtil.getTCountryTLanguages(countryId);
	}

}