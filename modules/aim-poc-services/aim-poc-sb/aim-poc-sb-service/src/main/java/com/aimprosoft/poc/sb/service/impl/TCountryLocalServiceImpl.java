/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service.impl;

import com.aimprosoft.poc.sb.model.TCountry;
import com.aimprosoft.poc.sb.model.TLanguage;
import com.aimprosoft.poc.sb.service.base.TCountryLocalServiceBaseImpl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;

import java.util.Map;

/**
 * The implementation of the t country local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.aimprosoft.poc.sb.service.TCountryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TCountryLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.aimprosoft.poc.sb.model.TCountry",
	service = AopService.class
)
public class TCountryLocalServiceImpl extends TCountryLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>com.aimprosoft.poc.sb.service.TCountryLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.aimprosoft.poc.sb.service.TCountryLocalServiceUtil</code>.
	 */

	public boolean countryExists(String countryCode) {
		return tCountryPersistence.countByCode(countryCode) > 0;
	}

	public TCountry getCountryByCode(String countryCode){
		if (Validator.isBlank(countryCode)) {
			return null;
		}
		countryCode = countryCode.toUpperCase();
		return tCountryPersistence.fetchByCode(countryCode);
	}

	public TCountry addTCountry(long companyId, String code, String name, String capital, String phoneCode, String continentCode,
								String currency, String flag, Map<String, String> languageMap) {

		long tCountryId = counterLocalService.increment();

		TCountry tCountry = tCountryPersistence.create(tCountryId);

		tCountry.setCompanyId(companyId);
		tCountry.setCode(code);
		tCountry.setName(name);
		tCountry.setCapital(capital);
		tCountry.setPhoneCode(phoneCode);
		tCountry.setContinentCode(continentCode);
		tCountry.setCurrency(currency);
		tCountry.setFlag(flag);

		tCountry = tCountryPersistence.update(tCountry);

		if (MapUtil.isNotEmpty(languageMap)) {
			for (String languageCode : languageMap.keySet()) {
				TLanguage tLanguage = tLanguagePersistence.fetchByCode(languageCode);
				if (tLanguage == null) {
					long tLanguageId = counterLocalService.increment();
					tLanguage = tLanguagePersistence.create(tLanguageId);
					tLanguage.setCompanyId(companyId);
					tLanguage.setCode(languageCode);
					String languageName = languageMap.get(languageCode);
					tLanguage.setName(languageName);
					tLanguage = tLanguagePersistence.update(tLanguage);
				}
				tCountryPersistence.addTLanguage(tCountryId, tLanguage);
			}
		}

		return tCountry;
	}

}