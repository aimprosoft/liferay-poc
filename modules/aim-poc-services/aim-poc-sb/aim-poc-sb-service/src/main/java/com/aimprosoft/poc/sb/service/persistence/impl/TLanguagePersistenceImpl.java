/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service.persistence.impl;

import com.aimprosoft.poc.sb.exception.NoSuchTLanguageException;
import com.aimprosoft.poc.sb.model.TCountry;
import com.aimprosoft.poc.sb.model.TLanguage;
import com.aimprosoft.poc.sb.model.impl.TLanguageImpl;
import com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl;
import com.aimprosoft.poc.sb.service.persistence.TLanguagePersistence;
import com.aimprosoft.poc.sb.service.persistence.impl.constants.aimPersistenceConstants;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the t language service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = TLanguagePersistence.class)
@ProviderType
public class TLanguagePersistenceImpl
	extends BasePersistenceImpl<TLanguage> implements TLanguagePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>TLanguageUtil</code> to access the t language persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		TLanguageImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathFetchByCode;
	private FinderPath _finderPathCountByCode;

	/**
	 * Returns the t language where code = &#63; or throws a <code>NoSuchTLanguageException</code> if it could not be found.
	 *
	 * @param code the code
	 * @return the matching t language
	 * @throws NoSuchTLanguageException if a matching t language could not be found
	 */
	@Override
	public TLanguage findByCode(String code) throws NoSuchTLanguageException {
		TLanguage tLanguage = fetchByCode(code);

		if (tLanguage == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("code=");
			msg.append(code);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchTLanguageException(msg.toString());
		}

		return tLanguage;
	}

	/**
	 * Returns the t language where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByCode(String)}
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t language, or <code>null</code> if a matching t language could not be found
	 */
	@Deprecated
	@Override
	public TLanguage fetchByCode(String code, boolean useFinderCache) {
		return fetchByCode(code);
	}

	/**
	 * Returns the t language where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t language, or <code>null</code> if a matching t language could not be found
	 */
	@Override
	public TLanguage fetchByCode(String code) {
		code = Objects.toString(code, "");

		Object[] finderArgs = new Object[] {code};

		Object result = finderCache.getResult(
			_finderPathFetchByCode, finderArgs, this);

		if (result instanceof TLanguage) {
			TLanguage tLanguage = (TLanguage)result;

			if (!Objects.equals(code, tLanguage.getCode())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TLANGUAGE_WHERE);

			boolean bindCode = false;

			if (code.isEmpty()) {
				query.append(_FINDER_COLUMN_CODE_CODE_3);
			}
			else {
				bindCode = true;

				query.append(_FINDER_COLUMN_CODE_CODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCode) {
					qPos.add(code);
				}

				List<TLanguage> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByCode, finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"TLanguagePersistenceImpl.fetchByCode(String, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					TLanguage tLanguage = list.get(0);

					result = tLanguage;

					cacheResult(tLanguage);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByCode, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TLanguage)result;
		}
	}

	/**
	 * Removes the t language where code = &#63; from the database.
	 *
	 * @param code the code
	 * @return the t language that was removed
	 */
	@Override
	public TLanguage removeByCode(String code) throws NoSuchTLanguageException {
		TLanguage tLanguage = findByCode(code);

		return remove(tLanguage);
	}

	/**
	 * Returns the number of t languages where code = &#63;.
	 *
	 * @param code the code
	 * @return the number of matching t languages
	 */
	@Override
	public int countByCode(String code) {
		code = Objects.toString(code, "");

		FinderPath finderPath = _finderPathCountByCode;

		Object[] finderArgs = new Object[] {code};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TLANGUAGE_WHERE);

			boolean bindCode = false;

			if (code.isEmpty()) {
				query.append(_FINDER_COLUMN_CODE_CODE_3);
			}
			else {
				bindCode = true;

				query.append(_FINDER_COLUMN_CODE_CODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCode) {
					qPos.add(code);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODE_CODE_2 =
		"tLanguage.code = ?";

	private static final String _FINDER_COLUMN_CODE_CODE_3 =
		"(tLanguage.code IS NULL OR tLanguage.code = '')";

	public TLanguagePersistenceImpl() {
		setModelClass(TLanguage.class);

		setModelImplClass(TLanguageImpl.class);
		setModelPKClass(long.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("code", "code_");

		setDBColumnNames(dbColumnNames);
	}

	/**
	 * Caches the t language in the entity cache if it is enabled.
	 *
	 * @param tLanguage the t language
	 */
	@Override
	public void cacheResult(TLanguage tLanguage) {
		entityCache.putResult(
			entityCacheEnabled, TLanguageImpl.class, tLanguage.getPrimaryKey(),
			tLanguage);

		finderCache.putResult(
			_finderPathFetchByCode, new Object[] {tLanguage.getCode()},
			tLanguage);

		tLanguage.resetOriginalValues();
	}

	/**
	 * Caches the t languages in the entity cache if it is enabled.
	 *
	 * @param tLanguages the t languages
	 */
	@Override
	public void cacheResult(List<TLanguage> tLanguages) {
		for (TLanguage tLanguage : tLanguages) {
			if (entityCache.getResult(
					entityCacheEnabled, TLanguageImpl.class,
					tLanguage.getPrimaryKey()) == null) {

				cacheResult(tLanguage);
			}
			else {
				tLanguage.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all t languages.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TLanguageImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the t language.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TLanguage tLanguage) {
		entityCache.removeResult(
			entityCacheEnabled, TLanguageImpl.class, tLanguage.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((TLanguageModelImpl)tLanguage, true);
	}

	@Override
	public void clearCache(List<TLanguage> tLanguages) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TLanguage tLanguage : tLanguages) {
			entityCache.removeResult(
				entityCacheEnabled, TLanguageImpl.class,
				tLanguage.getPrimaryKey());

			clearUniqueFindersCache((TLanguageModelImpl)tLanguage, true);
		}
	}

	protected void cacheUniqueFindersCache(
		TLanguageModelImpl tLanguageModelImpl) {

		Object[] args = new Object[] {tLanguageModelImpl.getCode()};

		finderCache.putResult(
			_finderPathCountByCode, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByCode, args, tLanguageModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		TLanguageModelImpl tLanguageModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {tLanguageModelImpl.getCode()};

			finderCache.removeResult(_finderPathCountByCode, args);
			finderCache.removeResult(_finderPathFetchByCode, args);
		}

		if ((tLanguageModelImpl.getColumnBitmask() &
			 _finderPathFetchByCode.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {tLanguageModelImpl.getOriginalCode()};

			finderCache.removeResult(_finderPathCountByCode, args);
			finderCache.removeResult(_finderPathFetchByCode, args);
		}
	}

	/**
	 * Creates a new t language with the primary key. Does not add the t language to the database.
	 *
	 * @param languageId the primary key for the new t language
	 * @return the new t language
	 */
	@Override
	public TLanguage create(long languageId) {
		TLanguage tLanguage = new TLanguageImpl();

		tLanguage.setNew(true);
		tLanguage.setPrimaryKey(languageId);

		tLanguage.setCompanyId(CompanyThreadLocal.getCompanyId());

		return tLanguage;
	}

	/**
	 * Removes the t language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language that was removed
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	@Override
	public TLanguage remove(long languageId) throws NoSuchTLanguageException {
		return remove((Serializable)languageId);
	}

	/**
	 * Removes the t language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the t language
	 * @return the t language that was removed
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	@Override
	public TLanguage remove(Serializable primaryKey)
		throws NoSuchTLanguageException {

		Session session = null;

		try {
			session = openSession();

			TLanguage tLanguage = (TLanguage)session.get(
				TLanguageImpl.class, primaryKey);

			if (tLanguage == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTLanguageException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(tLanguage);
		}
		catch (NoSuchTLanguageException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TLanguage removeImpl(TLanguage tLanguage) {
		tLanguageToTCountryTableMapper.deleteLeftPrimaryKeyTableMappings(
			tLanguage.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tLanguage)) {
				tLanguage = (TLanguage)session.get(
					TLanguageImpl.class, tLanguage.getPrimaryKeyObj());
			}

			if (tLanguage != null) {
				session.delete(tLanguage);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tLanguage != null) {
			clearCache(tLanguage);
		}

		return tLanguage;
	}

	@Override
	public TLanguage updateImpl(TLanguage tLanguage) {
		boolean isNew = tLanguage.isNew();

		if (!(tLanguage instanceof TLanguageModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(tLanguage.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(tLanguage);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in tLanguage proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom TLanguage implementation " +
					tLanguage.getClass());
		}

		TLanguageModelImpl tLanguageModelImpl = (TLanguageModelImpl)tLanguage;

		Session session = null;

		try {
			session = openSession();

			if (tLanguage.isNew()) {
				session.save(tLanguage);

				tLanguage.setNew(false);
			}
			else {
				tLanguage = (TLanguage)session.merge(tLanguage);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!_columnBitmaskEnabled) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, TLanguageImpl.class, tLanguage.getPrimaryKey(),
			tLanguage, false);

		clearUniqueFindersCache(tLanguageModelImpl, false);
		cacheUniqueFindersCache(tLanguageModelImpl);

		tLanguage.resetOriginalValues();

		return tLanguage;
	}

	/**
	 * Returns the t language with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the t language
	 * @return the t language
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	@Override
	public TLanguage findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTLanguageException {

		TLanguage tLanguage = fetchByPrimaryKey(primaryKey);

		if (tLanguage == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTLanguageException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return tLanguage;
	}

	/**
	 * Returns the t language with the primary key or throws a <code>NoSuchTLanguageException</code> if it could not be found.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	@Override
	public TLanguage findByPrimaryKey(long languageId)
		throws NoSuchTLanguageException {

		return findByPrimaryKey((Serializable)languageId);
	}

	/**
	 * Returns the t language with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language, or <code>null</code> if a t language with the primary key could not be found
	 */
	@Override
	public TLanguage fetchByPrimaryKey(long languageId) {
		return fetchByPrimaryKey((Serializable)languageId);
	}

	/**
	 * Returns all the t languages.
	 *
	 * @return the t languages
	 */
	@Override
	public List<TLanguage> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t languages
	 */
	@Override
	public List<TLanguage> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of t languages
	 */
	@Deprecated
	@Override
	public List<TLanguage> findAll(
		int start, int end, OrderByComparator<TLanguage> orderByComparator,
		boolean useFinderCache) {

		return findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t languages
	 */
	@Override
	public List<TLanguage> findAll(
		int start, int end, OrderByComparator<TLanguage> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<TLanguage> list = (List<TLanguage>)finderCache.getResult(
			finderPath, finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_TLANGUAGE);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TLANGUAGE;

				if (pagination) {
					sql = sql.concat(TLanguageModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TLanguage>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TLanguage>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the t languages from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (TLanguage tLanguage : findAll()) {
			remove(tLanguage);
		}
	}

	/**
	 * Returns the number of t languages.
	 *
	 * @return the number of t languages
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TLANGUAGE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of t countries associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return long[] of the primaryKeys of t countries associated with the t language
	 */
	@Override
	public long[] getTCountryPrimaryKeys(long pk) {
		long[] pks = tLanguageToTCountryTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return the t languages associated with the t country
	 */
	@Override
	public List<TLanguage> getTCountryTLanguages(long pk) {
		return getTCountryTLanguages(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t country
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t languages associated with the t country
	 */
	@Override
	public List<TLanguage> getTCountryTLanguages(long pk, int start, int end) {
		return getTCountryTLanguages(pk, start, end, null);
	}

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t country
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t languages associated with the t country
	 */
	@Override
	public List<TLanguage> getTCountryTLanguages(
		long pk, int start, int end,
		OrderByComparator<TLanguage> orderByComparator) {

		return tLanguageToTCountryTableMapper.getLeftBaseModels(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of t countries associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return the number of t countries associated with the t language
	 */
	@Override
	public int getTCountriesSize(long pk) {
		long[] pks = tLanguageToTCountryTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the t country is associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 * @return <code>true</code> if the t country is associated with the t language; <code>false</code> otherwise
	 */
	@Override
	public boolean containsTCountry(long pk, long tCountryPK) {
		return tLanguageToTCountryTableMapper.containsTableMapping(
			pk, tCountryPK);
	}

	/**
	 * Returns <code>true</code> if the t language has any t countries associated with it.
	 *
	 * @param pk the primary key of the t language to check for associations with t countries
	 * @return <code>true</code> if the t language has any t countries associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsTCountries(long pk) {
		if (getTCountriesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 */
	@Override
	public void addTCountry(long pk, long tCountryPK) {
		TLanguage tLanguage = fetchByPrimaryKey(pk);

		if (tLanguage == null) {
			tLanguageToTCountryTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, tCountryPK);
		}
		else {
			tLanguageToTCountryTableMapper.addTableMapping(
				tLanguage.getCompanyId(), pk, tCountryPK);
		}
	}

	/**
	 * Adds an association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountry the t country
	 */
	@Override
	public void addTCountry(long pk, TCountry tCountry) {
		TLanguage tLanguage = fetchByPrimaryKey(pk);

		if (tLanguage == null) {
			tLanguageToTCountryTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk,
				tCountry.getPrimaryKey());
		}
		else {
			tLanguageToTCountryTableMapper.addTableMapping(
				tLanguage.getCompanyId(), pk, tCountry.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries
	 */
	@Override
	public void addTCountries(long pk, long[] tCountryPKs) {
		long companyId = 0;

		TLanguage tLanguage = fetchByPrimaryKey(pk);

		if (tLanguage == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = tLanguage.getCompanyId();
		}

		tLanguageToTCountryTableMapper.addTableMappings(
			companyId, pk, tCountryPKs);
	}

	/**
	 * Adds an association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries
	 */
	@Override
	public void addTCountries(long pk, List<TCountry> tCountries) {
		addTCountries(
			pk, ListUtil.toLongArray(tCountries, TCountry.COUNTRY_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the t language and its t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language to clear the associated t countries from
	 */
	@Override
	public void clearTCountries(long pk) {
		tLanguageToTCountryTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 */
	@Override
	public void removeTCountry(long pk, long tCountryPK) {
		tLanguageToTCountryTableMapper.deleteTableMapping(pk, tCountryPK);
	}

	/**
	 * Removes the association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountry the t country
	 */
	@Override
	public void removeTCountry(long pk, TCountry tCountry) {
		tLanguageToTCountryTableMapper.deleteTableMapping(
			pk, tCountry.getPrimaryKey());
	}

	/**
	 * Removes the association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries
	 */
	@Override
	public void removeTCountries(long pk, long[] tCountryPKs) {
		tLanguageToTCountryTableMapper.deleteTableMappings(pk, tCountryPKs);
	}

	/**
	 * Removes the association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries
	 */
	@Override
	public void removeTCountries(long pk, List<TCountry> tCountries) {
		removeTCountries(
			pk, ListUtil.toLongArray(tCountries, TCountry.COUNTRY_ID_ACCESSOR));
	}

	/**
	 * Sets the t countries associated with the t language, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries to be associated with the t language
	 */
	@Override
	public void setTCountries(long pk, long[] tCountryPKs) {
		Set<Long> newTCountryPKsSet = SetUtil.fromArray(tCountryPKs);
		Set<Long> oldTCountryPKsSet = SetUtil.fromArray(
			tLanguageToTCountryTableMapper.getRightPrimaryKeys(pk));

		Set<Long> removeTCountryPKsSet = new HashSet<Long>(oldTCountryPKsSet);

		removeTCountryPKsSet.removeAll(newTCountryPKsSet);

		tLanguageToTCountryTableMapper.deleteTableMappings(
			pk, ArrayUtil.toLongArray(removeTCountryPKsSet));

		newTCountryPKsSet.removeAll(oldTCountryPKsSet);

		long companyId = 0;

		TLanguage tLanguage = fetchByPrimaryKey(pk);

		if (tLanguage == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = tLanguage.getCompanyId();
		}

		tLanguageToTCountryTableMapper.addTableMappings(
			companyId, pk, ArrayUtil.toLongArray(newTCountryPKsSet));
	}

	/**
	 * Sets the t countries associated with the t language, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries to be associated with the t language
	 */
	@Override
	public void setTCountries(long pk, List<TCountry> tCountries) {
		try {
			long[] tCountryPKs = new long[tCountries.size()];

			for (int i = 0; i < tCountries.size(); i++) {
				TCountry tCountry = tCountries.get(i);

				tCountryPKs[i] = tCountry.getPrimaryKey();
			}

			setTCountries(pk, tCountryPKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "languageId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_TLANGUAGE;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return TLanguageModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the t language persistence.
	 */
	@Activate
	public void activate() {
		TLanguageModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		TLanguageModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		tLanguageToTCountryTableMapper = TableMapperFactory.getTableMapper(
			"aim_TCountry_TLanguages#languageId", "aim_TCountry_TLanguages",
			"companyId", "languageId", "countryId", this, TCountry.class);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, TLanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, TLanguageImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathFetchByCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, TLanguageImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByCode",
			new String[] {String.class.getName()},
			TLanguageModelImpl.CODE_COLUMN_BITMASK);

		_finderPathCountByCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCode",
			new String[] {String.class.getName()});
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(TLanguageImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper(
			"aim_TCountry_TLanguages#languageId");
	}

	@Override
	@Reference(
		target = aimPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.com.aimprosoft.poc.sb.model.TLanguage"),
			true);
	}

	@Override
	@Reference(
		target = aimPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = aimPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	protected TableMapper<TLanguage, TCountry> tLanguageToTCountryTableMapper;

	private static final String _SQL_SELECT_TLANGUAGE =
		"SELECT tLanguage FROM TLanguage tLanguage";

	private static final String _SQL_SELECT_TLANGUAGE_WHERE =
		"SELECT tLanguage FROM TLanguage tLanguage WHERE ";

	private static final String _SQL_COUNT_TLANGUAGE =
		"SELECT COUNT(tLanguage) FROM TLanguage tLanguage";

	private static final String _SQL_COUNT_TLANGUAGE_WHERE =
		"SELECT COUNT(tLanguage) FROM TLanguage tLanguage WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "tLanguage.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No TLanguage exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No TLanguage exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		TLanguagePersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"code"});

	static {
		try {
			Class.forName(aimPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException cnfe) {
			throw new ExceptionInInitializerError(cnfe);
		}
	}

}