/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service.persistence.impl;

import com.aimprosoft.poc.sb.exception.NoSuchTCountryException;
import com.aimprosoft.poc.sb.model.TCountry;
import com.aimprosoft.poc.sb.model.TLanguage;
import com.aimprosoft.poc.sb.model.impl.TCountryImpl;
import com.aimprosoft.poc.sb.model.impl.TCountryModelImpl;
import com.aimprosoft.poc.sb.service.persistence.TCountryPersistence;
import com.aimprosoft.poc.sb.service.persistence.impl.constants.aimPersistenceConstants;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.service.persistence.impl.TableMapper;
import com.liferay.portal.kernel.service.persistence.impl.TableMapperFactory;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.sql.DataSource;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the t country service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = TCountryPersistence.class)
@ProviderType
public class TCountryPersistenceImpl
	extends BasePersistenceImpl<TCountry> implements TCountryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>TCountryUtil</code> to access the t country persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		TCountryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathFetchByCode;
	private FinderPath _finderPathCountByCode;

	/**
	 * Returns the t country where code = &#63; or throws a <code>NoSuchTCountryException</code> if it could not be found.
	 *
	 * @param code the code
	 * @return the matching t country
	 * @throws NoSuchTCountryException if a matching t country could not be found
	 */
	@Override
	public TCountry findByCode(String code) throws NoSuchTCountryException {
		TCountry tCountry = fetchByCode(code);

		if (tCountry == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("code=");
			msg.append(code);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchTCountryException(msg.toString());
		}

		return tCountry;
	}

	/**
	 * Returns the t country where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByCode(String)}
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t country, or <code>null</code> if a matching t country could not be found
	 */
	@Deprecated
	@Override
	public TCountry fetchByCode(String code, boolean useFinderCache) {
		return fetchByCode(code);
	}

	/**
	 * Returns the t country where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t country, or <code>null</code> if a matching t country could not be found
	 */
	@Override
	public TCountry fetchByCode(String code) {
		code = Objects.toString(code, "");

		Object[] finderArgs = new Object[] {code};

		Object result = finderCache.getResult(
			_finderPathFetchByCode, finderArgs, this);

		if (result instanceof TCountry) {
			TCountry tCountry = (TCountry)result;

			if (!Objects.equals(code, tCountry.getCode())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TCOUNTRY_WHERE);

			boolean bindCode = false;

			if (code.isEmpty()) {
				query.append(_FINDER_COLUMN_CODE_CODE_3);
			}
			else {
				bindCode = true;

				query.append(_FINDER_COLUMN_CODE_CODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCode) {
					qPos.add(code);
				}

				List<TCountry> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByCode, finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"TCountryPersistenceImpl.fetchByCode(String, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					TCountry tCountry = list.get(0);

					result = tCountry;

					cacheResult(tCountry);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByCode, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TCountry)result;
		}
	}

	/**
	 * Removes the t country where code = &#63; from the database.
	 *
	 * @param code the code
	 * @return the t country that was removed
	 */
	@Override
	public TCountry removeByCode(String code) throws NoSuchTCountryException {
		TCountry tCountry = findByCode(code);

		return remove(tCountry);
	}

	/**
	 * Returns the number of t countries where code = &#63;.
	 *
	 * @param code the code
	 * @return the number of matching t countries
	 */
	@Override
	public int countByCode(String code) {
		code = Objects.toString(code, "");

		FinderPath finderPath = _finderPathCountByCode;

		Object[] finderArgs = new Object[] {code};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TCOUNTRY_WHERE);

			boolean bindCode = false;

			if (code.isEmpty()) {
				query.append(_FINDER_COLUMN_CODE_CODE_3);
			}
			else {
				bindCode = true;

				query.append(_FINDER_COLUMN_CODE_CODE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCode) {
					qPos.add(code);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CODE_CODE_2 =
		"tCountry.code = ?";

	private static final String _FINDER_COLUMN_CODE_CODE_3 =
		"(tCountry.code IS NULL OR tCountry.code = '')";

	public TCountryPersistenceImpl() {
		setModelClass(TCountry.class);

		setModelImplClass(TCountryImpl.class);
		setModelPKClass(long.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("code", "code_");
		dbColumnNames.put("currency", "currency_");

		setDBColumnNames(dbColumnNames);
	}

	/**
	 * Caches the t country in the entity cache if it is enabled.
	 *
	 * @param tCountry the t country
	 */
	@Override
	public void cacheResult(TCountry tCountry) {
		entityCache.putResult(
			entityCacheEnabled, TCountryImpl.class, tCountry.getPrimaryKey(),
			tCountry);

		finderCache.putResult(
			_finderPathFetchByCode, new Object[] {tCountry.getCode()},
			tCountry);

		tCountry.resetOriginalValues();
	}

	/**
	 * Caches the t countries in the entity cache if it is enabled.
	 *
	 * @param tCountries the t countries
	 */
	@Override
	public void cacheResult(List<TCountry> tCountries) {
		for (TCountry tCountry : tCountries) {
			if (entityCache.getResult(
					entityCacheEnabled, TCountryImpl.class,
					tCountry.getPrimaryKey()) == null) {

				cacheResult(tCountry);
			}
			else {
				tCountry.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all t countries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TCountryImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the t country.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TCountry tCountry) {
		entityCache.removeResult(
			entityCacheEnabled, TCountryImpl.class, tCountry.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((TCountryModelImpl)tCountry, true);
	}

	@Override
	public void clearCache(List<TCountry> tCountries) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TCountry tCountry : tCountries) {
			entityCache.removeResult(
				entityCacheEnabled, TCountryImpl.class,
				tCountry.getPrimaryKey());

			clearUniqueFindersCache((TCountryModelImpl)tCountry, true);
		}
	}

	protected void cacheUniqueFindersCache(
		TCountryModelImpl tCountryModelImpl) {

		Object[] args = new Object[] {tCountryModelImpl.getCode()};

		finderCache.putResult(
			_finderPathCountByCode, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByCode, args, tCountryModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		TCountryModelImpl tCountryModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {tCountryModelImpl.getCode()};

			finderCache.removeResult(_finderPathCountByCode, args);
			finderCache.removeResult(_finderPathFetchByCode, args);
		}

		if ((tCountryModelImpl.getColumnBitmask() &
			 _finderPathFetchByCode.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {tCountryModelImpl.getOriginalCode()};

			finderCache.removeResult(_finderPathCountByCode, args);
			finderCache.removeResult(_finderPathFetchByCode, args);
		}
	}

	/**
	 * Creates a new t country with the primary key. Does not add the t country to the database.
	 *
	 * @param countryId the primary key for the new t country
	 * @return the new t country
	 */
	@Override
	public TCountry create(long countryId) {
		TCountry tCountry = new TCountryImpl();

		tCountry.setNew(true);
		tCountry.setPrimaryKey(countryId);

		tCountry.setCompanyId(CompanyThreadLocal.getCompanyId());

		return tCountry;
	}

	/**
	 * Removes the t country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country that was removed
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	@Override
	public TCountry remove(long countryId) throws NoSuchTCountryException {
		return remove((Serializable)countryId);
	}

	/**
	 * Removes the t country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the t country
	 * @return the t country that was removed
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	@Override
	public TCountry remove(Serializable primaryKey)
		throws NoSuchTCountryException {

		Session session = null;

		try {
			session = openSession();

			TCountry tCountry = (TCountry)session.get(
				TCountryImpl.class, primaryKey);

			if (tCountry == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTCountryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(tCountry);
		}
		catch (NoSuchTCountryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TCountry removeImpl(TCountry tCountry) {
		tCountryToTLanguageTableMapper.deleteLeftPrimaryKeyTableMappings(
			tCountry.getPrimaryKey());

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tCountry)) {
				tCountry = (TCountry)session.get(
					TCountryImpl.class, tCountry.getPrimaryKeyObj());
			}

			if (tCountry != null) {
				session.delete(tCountry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tCountry != null) {
			clearCache(tCountry);
		}

		return tCountry;
	}

	@Override
	public TCountry updateImpl(TCountry tCountry) {
		boolean isNew = tCountry.isNew();

		if (!(tCountry instanceof TCountryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(tCountry.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(tCountry);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in tCountry proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom TCountry implementation " +
					tCountry.getClass());
		}

		TCountryModelImpl tCountryModelImpl = (TCountryModelImpl)tCountry;

		Session session = null;

		try {
			session = openSession();

			if (tCountry.isNew()) {
				session.save(tCountry);

				tCountry.setNew(false);
			}
			else {
				tCountry = (TCountry)session.merge(tCountry);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!_columnBitmaskEnabled) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(
			entityCacheEnabled, TCountryImpl.class, tCountry.getPrimaryKey(),
			tCountry, false);

		clearUniqueFindersCache(tCountryModelImpl, false);
		cacheUniqueFindersCache(tCountryModelImpl);

		tCountry.resetOriginalValues();

		return tCountry;
	}

	/**
	 * Returns the t country with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the t country
	 * @return the t country
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	@Override
	public TCountry findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTCountryException {

		TCountry tCountry = fetchByPrimaryKey(primaryKey);

		if (tCountry == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTCountryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return tCountry;
	}

	/**
	 * Returns the t country with the primary key or throws a <code>NoSuchTCountryException</code> if it could not be found.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	@Override
	public TCountry findByPrimaryKey(long countryId)
		throws NoSuchTCountryException {

		return findByPrimaryKey((Serializable)countryId);
	}

	/**
	 * Returns the t country with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country, or <code>null</code> if a t country with the primary key could not be found
	 */
	@Override
	public TCountry fetchByPrimaryKey(long countryId) {
		return fetchByPrimaryKey((Serializable)countryId);
	}

	/**
	 * Returns all the t countries.
	 *
	 * @return the t countries
	 */
	@Override
	public List<TCountry> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t countries
	 */
	@Override
	public List<TCountry> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of t countries
	 */
	@Deprecated
	@Override
	public List<TCountry> findAll(
		int start, int end, OrderByComparator<TCountry> orderByComparator,
		boolean useFinderCache) {

		return findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t countries
	 */
	@Override
	public List<TCountry> findAll(
		int start, int end, OrderByComparator<TCountry> orderByComparator) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<TCountry> list = (List<TCountry>)finderCache.getResult(
			finderPath, finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_TCOUNTRY);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TCOUNTRY;

				if (pagination) {
					sql = sql.concat(TCountryModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TCountry>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TCountry>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the t countries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (TCountry tCountry : findAll()) {
			remove(tCountry);
		}
	}

	/**
	 * Returns the number of t countries.
	 *
	 * @return the number of t countries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TCOUNTRY);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the primaryKeys of t languages associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return long[] of the primaryKeys of t languages associated with the t country
	 */
	@Override
	public long[] getTLanguagePrimaryKeys(long pk) {
		long[] pks = tCountryToTLanguageTableMapper.getRightPrimaryKeys(pk);

		return pks.clone();
	}

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return the t countries associated with the t language
	 */
	@Override
	public List<TCountry> getTLanguageTCountries(long pk) {
		return getTLanguageTCountries(pk, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	}

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t language
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t countries associated with the t language
	 */
	@Override
	public List<TCountry> getTLanguageTCountries(long pk, int start, int end) {
		return getTLanguageTCountries(pk, start, end, null);
	}

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t language
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t countries associated with the t language
	 */
	@Override
	public List<TCountry> getTLanguageTCountries(
		long pk, int start, int end,
		OrderByComparator<TCountry> orderByComparator) {

		return tCountryToTLanguageTableMapper.getLeftBaseModels(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of t languages associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return the number of t languages associated with the t country
	 */
	@Override
	public int getTLanguagesSize(long pk) {
		long[] pks = tCountryToTLanguageTableMapper.getRightPrimaryKeys(pk);

		return pks.length;
	}

	/**
	 * Returns <code>true</code> if the t language is associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 * @return <code>true</code> if the t language is associated with the t country; <code>false</code> otherwise
	 */
	@Override
	public boolean containsTLanguage(long pk, long tLanguagePK) {
		return tCountryToTLanguageTableMapper.containsTableMapping(
			pk, tLanguagePK);
	}

	/**
	 * Returns <code>true</code> if the t country has any t languages associated with it.
	 *
	 * @param pk the primary key of the t country to check for associations with t languages
	 * @return <code>true</code> if the t country has any t languages associated with it; <code>false</code> otherwise
	 */
	@Override
	public boolean containsTLanguages(long pk) {
		if (getTLanguagesSize(pk) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Adds an association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 */
	@Override
	public void addTLanguage(long pk, long tLanguagePK) {
		TCountry tCountry = fetchByPrimaryKey(pk);

		if (tCountry == null) {
			tCountryToTLanguageTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk, tLanguagePK);
		}
		else {
			tCountryToTLanguageTableMapper.addTableMapping(
				tCountry.getCompanyId(), pk, tLanguagePK);
		}
	}

	/**
	 * Adds an association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguage the t language
	 */
	@Override
	public void addTLanguage(long pk, TLanguage tLanguage) {
		TCountry tCountry = fetchByPrimaryKey(pk);

		if (tCountry == null) {
			tCountryToTLanguageTableMapper.addTableMapping(
				CompanyThreadLocal.getCompanyId(), pk,
				tLanguage.getPrimaryKey());
		}
		else {
			tCountryToTLanguageTableMapper.addTableMapping(
				tCountry.getCompanyId(), pk, tLanguage.getPrimaryKey());
		}
	}

	/**
	 * Adds an association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages
	 */
	@Override
	public void addTLanguages(long pk, long[] tLanguagePKs) {
		long companyId = 0;

		TCountry tCountry = fetchByPrimaryKey(pk);

		if (tCountry == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = tCountry.getCompanyId();
		}

		tCountryToTLanguageTableMapper.addTableMappings(
			companyId, pk, tLanguagePKs);
	}

	/**
	 * Adds an association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages
	 */
	@Override
	public void addTLanguages(long pk, List<TLanguage> tLanguages) {
		addTLanguages(
			pk,
			ListUtil.toLongArray(tLanguages, TLanguage.LANGUAGE_ID_ACCESSOR));
	}

	/**
	 * Clears all associations between the t country and its t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country to clear the associated t languages from
	 */
	@Override
	public void clearTLanguages(long pk) {
		tCountryToTLanguageTableMapper.deleteLeftPrimaryKeyTableMappings(pk);
	}

	/**
	 * Removes the association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 */
	@Override
	public void removeTLanguage(long pk, long tLanguagePK) {
		tCountryToTLanguageTableMapper.deleteTableMapping(pk, tLanguagePK);
	}

	/**
	 * Removes the association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguage the t language
	 */
	@Override
	public void removeTLanguage(long pk, TLanguage tLanguage) {
		tCountryToTLanguageTableMapper.deleteTableMapping(
			pk, tLanguage.getPrimaryKey());
	}

	/**
	 * Removes the association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages
	 */
	@Override
	public void removeTLanguages(long pk, long[] tLanguagePKs) {
		tCountryToTLanguageTableMapper.deleteTableMappings(pk, tLanguagePKs);
	}

	/**
	 * Removes the association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages
	 */
	@Override
	public void removeTLanguages(long pk, List<TLanguage> tLanguages) {
		removeTLanguages(
			pk,
			ListUtil.toLongArray(tLanguages, TLanguage.LANGUAGE_ID_ACCESSOR));
	}

	/**
	 * Sets the t languages associated with the t country, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages to be associated with the t country
	 */
	@Override
	public void setTLanguages(long pk, long[] tLanguagePKs) {
		Set<Long> newTLanguagePKsSet = SetUtil.fromArray(tLanguagePKs);
		Set<Long> oldTLanguagePKsSet = SetUtil.fromArray(
			tCountryToTLanguageTableMapper.getRightPrimaryKeys(pk));

		Set<Long> removeTLanguagePKsSet = new HashSet<Long>(oldTLanguagePKsSet);

		removeTLanguagePKsSet.removeAll(newTLanguagePKsSet);

		tCountryToTLanguageTableMapper.deleteTableMappings(
			pk, ArrayUtil.toLongArray(removeTLanguagePKsSet));

		newTLanguagePKsSet.removeAll(oldTLanguagePKsSet);

		long companyId = 0;

		TCountry tCountry = fetchByPrimaryKey(pk);

		if (tCountry == null) {
			companyId = CompanyThreadLocal.getCompanyId();
		}
		else {
			companyId = tCountry.getCompanyId();
		}

		tCountryToTLanguageTableMapper.addTableMappings(
			companyId, pk, ArrayUtil.toLongArray(newTLanguagePKsSet));
	}

	/**
	 * Sets the t languages associated with the t country, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages to be associated with the t country
	 */
	@Override
	public void setTLanguages(long pk, List<TLanguage> tLanguages) {
		try {
			long[] tLanguagePKs = new long[tLanguages.size()];

			for (int i = 0; i < tLanguages.size(); i++) {
				TLanguage tLanguage = tLanguages.get(i);

				tLanguagePKs[i] = tLanguage.getPrimaryKey();
			}

			setTLanguages(pk, tLanguagePKs);
		}
		catch (Exception e) {
			throw processException(e);
		}
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "countryId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_TCOUNTRY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return TCountryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the t country persistence.
	 */
	@Activate
	public void activate() {
		TCountryModelImpl.setEntityCacheEnabled(entityCacheEnabled);
		TCountryModelImpl.setFinderCacheEnabled(finderCacheEnabled);

		tCountryToTLanguageTableMapper = TableMapperFactory.getTableMapper(
			"aim_TCountry_TLanguages#countryId", "aim_TCountry_TLanguages",
			"companyId", "countryId", "languageId", this, TLanguage.class);

		_finderPathWithPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, TCountryImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, TCountryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathFetchByCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, TCountryImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByCode",
			new String[] {String.class.getName()},
			TCountryModelImpl.CODE_COLUMN_BITMASK);

		_finderPathCountByCode = new FinderPath(
			entityCacheEnabled, finderCacheEnabled, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCode",
			new String[] {String.class.getName()});
	}

	@Deactivate
	public void deactivate() {
		entityCache.removeCache(TCountryImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		TableMapperFactory.removeTableMapper(
			"aim_TCountry_TLanguages#countryId");
	}

	@Override
	@Reference(
		target = aimPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
		super.setConfiguration(configuration);

		_columnBitmaskEnabled = GetterUtil.getBoolean(
			configuration.get(
				"value.object.column.bitmask.enabled.com.aimprosoft.poc.sb.model.TCountry"),
			true);
	}

	@Override
	@Reference(
		target = aimPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = aimPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private boolean _columnBitmaskEnabled;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	protected TableMapper<TCountry, TLanguage> tCountryToTLanguageTableMapper;

	private static final String _SQL_SELECT_TCOUNTRY =
		"SELECT tCountry FROM TCountry tCountry";

	private static final String _SQL_SELECT_TCOUNTRY_WHERE =
		"SELECT tCountry FROM TCountry tCountry WHERE ";

	private static final String _SQL_COUNT_TCOUNTRY =
		"SELECT COUNT(tCountry) FROM TCountry tCountry";

	private static final String _SQL_COUNT_TCOUNTRY_WHERE =
		"SELECT COUNT(tCountry) FROM TCountry tCountry WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "tCountry.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No TCountry exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No TCountry exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		TCountryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"code", "currency"});

	static {
		try {
			Class.forName(aimPersistenceConstants.class.getName());
		}
		catch (ClassNotFoundException cnfe) {
			throw new ExceptionInInitializerError(cnfe);
		}
	}

}