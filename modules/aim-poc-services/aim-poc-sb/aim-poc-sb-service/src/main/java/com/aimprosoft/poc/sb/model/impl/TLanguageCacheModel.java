/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.model.impl;

import com.aimprosoft.poc.sb.model.TLanguage;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing TLanguage in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class TLanguageCacheModel
	implements CacheModel<TLanguage>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TLanguageCacheModel)) {
			return false;
		}

		TLanguageCacheModel tLanguageCacheModel = (TLanguageCacheModel)obj;

		if (languageId == tLanguageCacheModel.languageId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, languageId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{languageId=");
		sb.append(languageId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", code=");
		sb.append(code);
		sb.append(", name=");
		sb.append(name);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TLanguage toEntityModel() {
		TLanguageImpl tLanguageImpl = new TLanguageImpl();

		tLanguageImpl.setLanguageId(languageId);
		tLanguageImpl.setCompanyId(companyId);

		if (code == null) {
			tLanguageImpl.setCode("");
		}
		else {
			tLanguageImpl.setCode(code);
		}

		if (name == null) {
			tLanguageImpl.setName("");
		}
		else {
			tLanguageImpl.setName(name);
		}

		tLanguageImpl.resetOriginalValues();

		return tLanguageImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		languageId = objectInput.readLong();

		companyId = objectInput.readLong();
		code = objectInput.readUTF();
		name = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(languageId);

		objectOutput.writeLong(companyId);

		if (code == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(code);
		}

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}
	}

	public long languageId;
	public long companyId;
	public String code;
	public String name;

}