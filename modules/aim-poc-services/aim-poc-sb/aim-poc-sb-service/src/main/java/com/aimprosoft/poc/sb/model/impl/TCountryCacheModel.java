/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.model.impl;

import com.aimprosoft.poc.sb.model.TCountry;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The cache model class for representing TCountry in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class TCountryCacheModel
	implements CacheModel<TCountry>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TCountryCacheModel)) {
			return false;
		}

		TCountryCacheModel tCountryCacheModel = (TCountryCacheModel)obj;

		if (countryId == tCountryCacheModel.countryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, countryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{countryId=");
		sb.append(countryId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", code=");
		sb.append(code);
		sb.append(", name=");
		sb.append(name);
		sb.append(", capital=");
		sb.append(capital);
		sb.append(", phoneCode=");
		sb.append(phoneCode);
		sb.append(", continentCode=");
		sb.append(continentCode);
		sb.append(", currency=");
		sb.append(currency);
		sb.append(", flag=");
		sb.append(flag);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TCountry toEntityModel() {
		TCountryImpl tCountryImpl = new TCountryImpl();

		tCountryImpl.setCountryId(countryId);
		tCountryImpl.setCompanyId(companyId);

		if (code == null) {
			tCountryImpl.setCode("");
		}
		else {
			tCountryImpl.setCode(code);
		}

		if (name == null) {
			tCountryImpl.setName("");
		}
		else {
			tCountryImpl.setName(name);
		}

		if (capital == null) {
			tCountryImpl.setCapital("");
		}
		else {
			tCountryImpl.setCapital(capital);
		}

		if (phoneCode == null) {
			tCountryImpl.setPhoneCode("");
		}
		else {
			tCountryImpl.setPhoneCode(phoneCode);
		}

		if (continentCode == null) {
			tCountryImpl.setContinentCode("");
		}
		else {
			tCountryImpl.setContinentCode(continentCode);
		}

		if (currency == null) {
			tCountryImpl.setCurrency("");
		}
		else {
			tCountryImpl.setCurrency(currency);
		}

		if (flag == null) {
			tCountryImpl.setFlag("");
		}
		else {
			tCountryImpl.setFlag(flag);
		}

		tCountryImpl.resetOriginalValues();

		return tCountryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		countryId = objectInput.readLong();

		companyId = objectInput.readLong();
		code = objectInput.readUTF();
		name = objectInput.readUTF();
		capital = objectInput.readUTF();
		phoneCode = objectInput.readUTF();
		continentCode = objectInput.readUTF();
		currency = objectInput.readUTF();
		flag = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeLong(countryId);

		objectOutput.writeLong(companyId);

		if (code == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(code);
		}

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (capital == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(capital);
		}

		if (phoneCode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(phoneCode);
		}

		if (continentCode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(continentCode);
		}

		if (currency == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(currency);
		}

		if (flag == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(flag);
		}
	}

	public long countryId;
	public long companyId;
	public String code;
	public String name;
	public String capital;
	public String phoneCode;
	public String continentCode;
	public String currency;
	public String flag;

}