/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service.persistence;

import com.aimprosoft.poc.sb.model.TLanguage;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the t language service. This utility wraps <code>com.aimprosoft.poc.sb.service.persistence.impl.TLanguagePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TLanguagePersistence
 * @generated
 */
@ProviderType
public class TLanguageUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(TLanguage tLanguage) {
		getPersistence().clearCache(tLanguage);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, TLanguage> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TLanguage> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TLanguage> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TLanguage> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<TLanguage> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static TLanguage update(TLanguage tLanguage) {
		return getPersistence().update(tLanguage);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static TLanguage update(
		TLanguage tLanguage, ServiceContext serviceContext) {

		return getPersistence().update(tLanguage, serviceContext);
	}

	/**
	 * Returns the t language where code = &#63; or throws a <code>NoSuchTLanguageException</code> if it could not be found.
	 *
	 * @param code the code
	 * @return the matching t language
	 * @throws NoSuchTLanguageException if a matching t language could not be found
	 */
	public static TLanguage findByCode(String code)
		throws com.aimprosoft.poc.sb.exception.NoSuchTLanguageException {

		return getPersistence().findByCode(code);
	}

	/**
	 * Returns the t language where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByCode(String)}
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t language, or <code>null</code> if a matching t language could not be found
	 */
	@Deprecated
	public static TLanguage fetchByCode(String code, boolean useFinderCache) {
		return getPersistence().fetchByCode(code, useFinderCache);
	}

	/**
	 * Returns the t language where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t language, or <code>null</code> if a matching t language could not be found
	 */
	public static TLanguage fetchByCode(String code) {
		return getPersistence().fetchByCode(code);
	}

	/**
	 * Removes the t language where code = &#63; from the database.
	 *
	 * @param code the code
	 * @return the t language that was removed
	 */
	public static TLanguage removeByCode(String code)
		throws com.aimprosoft.poc.sb.exception.NoSuchTLanguageException {

		return getPersistence().removeByCode(code);
	}

	/**
	 * Returns the number of t languages where code = &#63;.
	 *
	 * @param code the code
	 * @return the number of matching t languages
	 */
	public static int countByCode(String code) {
		return getPersistence().countByCode(code);
	}

	/**
	 * Caches the t language in the entity cache if it is enabled.
	 *
	 * @param tLanguage the t language
	 */
	public static void cacheResult(TLanguage tLanguage) {
		getPersistence().cacheResult(tLanguage);
	}

	/**
	 * Caches the t languages in the entity cache if it is enabled.
	 *
	 * @param tLanguages the t languages
	 */
	public static void cacheResult(List<TLanguage> tLanguages) {
		getPersistence().cacheResult(tLanguages);
	}

	/**
	 * Creates a new t language with the primary key. Does not add the t language to the database.
	 *
	 * @param languageId the primary key for the new t language
	 * @return the new t language
	 */
	public static TLanguage create(long languageId) {
		return getPersistence().create(languageId);
	}

	/**
	 * Removes the t language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language that was removed
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	public static TLanguage remove(long languageId)
		throws com.aimprosoft.poc.sb.exception.NoSuchTLanguageException {

		return getPersistence().remove(languageId);
	}

	public static TLanguage updateImpl(TLanguage tLanguage) {
		return getPersistence().updateImpl(tLanguage);
	}

	/**
	 * Returns the t language with the primary key or throws a <code>NoSuchTLanguageException</code> if it could not be found.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	public static TLanguage findByPrimaryKey(long languageId)
		throws com.aimprosoft.poc.sb.exception.NoSuchTLanguageException {

		return getPersistence().findByPrimaryKey(languageId);
	}

	/**
	 * Returns the t language with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language, or <code>null</code> if a t language with the primary key could not be found
	 */
	public static TLanguage fetchByPrimaryKey(long languageId) {
		return getPersistence().fetchByPrimaryKey(languageId);
	}

	/**
	 * Returns all the t languages.
	 *
	 * @return the t languages
	 */
	public static List<TLanguage> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t languages
	 */
	public static List<TLanguage> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of t languages
	 */
	@Deprecated
	public static List<TLanguage> findAll(
		int start, int end, OrderByComparator<TLanguage> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t languages
	 */
	public static List<TLanguage> findAll(
		int start, int end, OrderByComparator<TLanguage> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Removes all the t languages from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of t languages.
	 *
	 * @return the number of t languages
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	 * Returns the primaryKeys of t countries associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return long[] of the primaryKeys of t countries associated with the t language
	 */
	public static long[] getTCountryPrimaryKeys(long pk) {
		return getPersistence().getTCountryPrimaryKeys(pk);
	}

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return the t languages associated with the t country
	 */
	public static List<TLanguage> getTCountryTLanguages(long pk) {
		return getPersistence().getTCountryTLanguages(pk);
	}

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t country
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t languages associated with the t country
	 */
	public static List<TLanguage> getTCountryTLanguages(
		long pk, int start, int end) {

		return getPersistence().getTCountryTLanguages(pk, start, end);
	}

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t country
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t languages associated with the t country
	 */
	public static List<TLanguage> getTCountryTLanguages(
		long pk, int start, int end,
		OrderByComparator<TLanguage> orderByComparator) {

		return getPersistence().getTCountryTLanguages(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of t countries associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return the number of t countries associated with the t language
	 */
	public static int getTCountriesSize(long pk) {
		return getPersistence().getTCountriesSize(pk);
	}

	/**
	 * Returns <code>true</code> if the t country is associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 * @return <code>true</code> if the t country is associated with the t language; <code>false</code> otherwise
	 */
	public static boolean containsTCountry(long pk, long tCountryPK) {
		return getPersistence().containsTCountry(pk, tCountryPK);
	}

	/**
	 * Returns <code>true</code> if the t language has any t countries associated with it.
	 *
	 * @param pk the primary key of the t language to check for associations with t countries
	 * @return <code>true</code> if the t language has any t countries associated with it; <code>false</code> otherwise
	 */
	public static boolean containsTCountries(long pk) {
		return getPersistence().containsTCountries(pk);
	}

	/**
	 * Adds an association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 */
	public static void addTCountry(long pk, long tCountryPK) {
		getPersistence().addTCountry(pk, tCountryPK);
	}

	/**
	 * Adds an association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountry the t country
	 */
	public static void addTCountry(
		long pk, com.aimprosoft.poc.sb.model.TCountry tCountry) {

		getPersistence().addTCountry(pk, tCountry);
	}

	/**
	 * Adds an association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries
	 */
	public static void addTCountries(long pk, long[] tCountryPKs) {
		getPersistence().addTCountries(pk, tCountryPKs);
	}

	/**
	 * Adds an association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries
	 */
	public static void addTCountries(
		long pk, List<com.aimprosoft.poc.sb.model.TCountry> tCountries) {

		getPersistence().addTCountries(pk, tCountries);
	}

	/**
	 * Clears all associations between the t language and its t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language to clear the associated t countries from
	 */
	public static void clearTCountries(long pk) {
		getPersistence().clearTCountries(pk);
	}

	/**
	 * Removes the association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 */
	public static void removeTCountry(long pk, long tCountryPK) {
		getPersistence().removeTCountry(pk, tCountryPK);
	}

	/**
	 * Removes the association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountry the t country
	 */
	public static void removeTCountry(
		long pk, com.aimprosoft.poc.sb.model.TCountry tCountry) {

		getPersistence().removeTCountry(pk, tCountry);
	}

	/**
	 * Removes the association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries
	 */
	public static void removeTCountries(long pk, long[] tCountryPKs) {
		getPersistence().removeTCountries(pk, tCountryPKs);
	}

	/**
	 * Removes the association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries
	 */
	public static void removeTCountries(
		long pk, List<com.aimprosoft.poc.sb.model.TCountry> tCountries) {

		getPersistence().removeTCountries(pk, tCountries);
	}

	/**
	 * Sets the t countries associated with the t language, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries to be associated with the t language
	 */
	public static void setTCountries(long pk, long[] tCountryPKs) {
		getPersistence().setTCountries(pk, tCountryPKs);
	}

	/**
	 * Sets the t countries associated with the t language, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries to be associated with the t language
	 */
	public static void setTCountries(
		long pk, List<com.aimprosoft.poc.sb.model.TCountry> tCountries) {

		getPersistence().setTCountries(pk, tCountries);
	}

	public static TLanguagePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TLanguagePersistence, TLanguagePersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(TLanguagePersistence.class);

		ServiceTracker<TLanguagePersistence, TLanguagePersistence>
			serviceTracker =
				new ServiceTracker<TLanguagePersistence, TLanguagePersistence>(
					bundle.getBundleContext(), TLanguagePersistence.class,
					null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}