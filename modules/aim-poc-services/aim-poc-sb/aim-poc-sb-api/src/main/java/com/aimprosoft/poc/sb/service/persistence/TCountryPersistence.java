/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service.persistence;

import com.aimprosoft.poc.sb.exception.NoSuchTCountryException;
import com.aimprosoft.poc.sb.model.TCountry;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the t country service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TCountryUtil
 * @generated
 */
@ProviderType
public interface TCountryPersistence extends BasePersistence<TCountry> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TCountryUtil} to access the t country persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns the t country where code = &#63; or throws a <code>NoSuchTCountryException</code> if it could not be found.
	 *
	 * @param code the code
	 * @return the matching t country
	 * @throws NoSuchTCountryException if a matching t country could not be found
	 */
	public TCountry findByCode(String code) throws NoSuchTCountryException;

	/**
	 * Returns the t country where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByCode(String)}
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t country, or <code>null</code> if a matching t country could not be found
	 */
	@Deprecated
	public TCountry fetchByCode(String code, boolean useFinderCache);

	/**
	 * Returns the t country where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t country, or <code>null</code> if a matching t country could not be found
	 */
	public TCountry fetchByCode(String code);

	/**
	 * Removes the t country where code = &#63; from the database.
	 *
	 * @param code the code
	 * @return the t country that was removed
	 */
	public TCountry removeByCode(String code) throws NoSuchTCountryException;

	/**
	 * Returns the number of t countries where code = &#63;.
	 *
	 * @param code the code
	 * @return the number of matching t countries
	 */
	public int countByCode(String code);

	/**
	 * Caches the t country in the entity cache if it is enabled.
	 *
	 * @param tCountry the t country
	 */
	public void cacheResult(TCountry tCountry);

	/**
	 * Caches the t countries in the entity cache if it is enabled.
	 *
	 * @param tCountries the t countries
	 */
	public void cacheResult(java.util.List<TCountry> tCountries);

	/**
	 * Creates a new t country with the primary key. Does not add the t country to the database.
	 *
	 * @param countryId the primary key for the new t country
	 * @return the new t country
	 */
	public TCountry create(long countryId);

	/**
	 * Removes the t country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country that was removed
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	public TCountry remove(long countryId) throws NoSuchTCountryException;

	public TCountry updateImpl(TCountry tCountry);

	/**
	 * Returns the t country with the primary key or throws a <code>NoSuchTCountryException</code> if it could not be found.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	public TCountry findByPrimaryKey(long countryId)
		throws NoSuchTCountryException;

	/**
	 * Returns the t country with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country, or <code>null</code> if a t country with the primary key could not be found
	 */
	public TCountry fetchByPrimaryKey(long countryId);

	/**
	 * Returns all the t countries.
	 *
	 * @return the t countries
	 */
	public java.util.List<TCountry> findAll();

	/**
	 * Returns a range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t countries
	 */
	public java.util.List<TCountry> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of t countries
	 */
	@Deprecated
	public java.util.List<TCountry> findAll(
		int start, int end, OrderByComparator<TCountry> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t countries
	 */
	public java.util.List<TCountry> findAll(
		int start, int end, OrderByComparator<TCountry> orderByComparator);

	/**
	 * Removes all the t countries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of t countries.
	 *
	 * @return the number of t countries
	 */
	public int countAll();

	/**
	 * Returns the primaryKeys of t languages associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return long[] of the primaryKeys of t languages associated with the t country
	 */
	public long[] getTLanguagePrimaryKeys(long pk);

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return the t countries associated with the t language
	 */
	public java.util.List<TCountry> getTLanguageTCountries(long pk);

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t language
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t countries associated with the t language
	 */
	public java.util.List<TCountry> getTLanguageTCountries(
		long pk, int start, int end);

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t language
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t countries associated with the t language
	 */
	public java.util.List<TCountry> getTLanguageTCountries(
		long pk, int start, int end,
		OrderByComparator<TCountry> orderByComparator);

	/**
	 * Returns the number of t languages associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return the number of t languages associated with the t country
	 */
	public int getTLanguagesSize(long pk);

	/**
	 * Returns <code>true</code> if the t language is associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 * @return <code>true</code> if the t language is associated with the t country; <code>false</code> otherwise
	 */
	public boolean containsTLanguage(long pk, long tLanguagePK);

	/**
	 * Returns <code>true</code> if the t country has any t languages associated with it.
	 *
	 * @param pk the primary key of the t country to check for associations with t languages
	 * @return <code>true</code> if the t country has any t languages associated with it; <code>false</code> otherwise
	 */
	public boolean containsTLanguages(long pk);

	/**
	 * Adds an association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 */
	public void addTLanguage(long pk, long tLanguagePK);

	/**
	 * Adds an association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguage the t language
	 */
	public void addTLanguage(
		long pk, com.aimprosoft.poc.sb.model.TLanguage tLanguage);

	/**
	 * Adds an association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages
	 */
	public void addTLanguages(long pk, long[] tLanguagePKs);

	/**
	 * Adds an association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages
	 */
	public void addTLanguages(
		long pk,
		java.util.List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages);

	/**
	 * Clears all associations between the t country and its t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country to clear the associated t languages from
	 */
	public void clearTLanguages(long pk);

	/**
	 * Removes the association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 */
	public void removeTLanguage(long pk, long tLanguagePK);

	/**
	 * Removes the association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguage the t language
	 */
	public void removeTLanguage(
		long pk, com.aimprosoft.poc.sb.model.TLanguage tLanguage);

	/**
	 * Removes the association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages
	 */
	public void removeTLanguages(long pk, long[] tLanguagePKs);

	/**
	 * Removes the association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages
	 */
	public void removeTLanguages(
		long pk,
		java.util.List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages);

	/**
	 * Sets the t languages associated with the t country, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages to be associated with the t country
	 */
	public void setTLanguages(long pk, long[] tLanguagePKs);

	/**
	 * Sets the t languages associated with the t country, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages to be associated with the t country
	 */
	public void setTLanguages(
		long pk,
		java.util.List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages);

}