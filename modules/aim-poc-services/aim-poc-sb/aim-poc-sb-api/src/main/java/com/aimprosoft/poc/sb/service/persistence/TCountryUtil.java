/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service.persistence;

import com.aimprosoft.poc.sb.model.TCountry;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the t country service. This utility wraps <code>com.aimprosoft.poc.sb.service.persistence.impl.TCountryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TCountryPersistence
 * @generated
 */
@ProviderType
public class TCountryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(TCountry tCountry) {
		getPersistence().clearCache(tCountry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, TCountry> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TCountry> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TCountry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TCountry> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<TCountry> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static TCountry update(TCountry tCountry) {
		return getPersistence().update(tCountry);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static TCountry update(
		TCountry tCountry, ServiceContext serviceContext) {

		return getPersistence().update(tCountry, serviceContext);
	}

	/**
	 * Returns the t country where code = &#63; or throws a <code>NoSuchTCountryException</code> if it could not be found.
	 *
	 * @param code the code
	 * @return the matching t country
	 * @throws NoSuchTCountryException if a matching t country could not be found
	 */
	public static TCountry findByCode(String code)
		throws com.aimprosoft.poc.sb.exception.NoSuchTCountryException {

		return getPersistence().findByCode(code);
	}

	/**
	 * Returns the t country where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByCode(String)}
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t country, or <code>null</code> if a matching t country could not be found
	 */
	@Deprecated
	public static TCountry fetchByCode(String code, boolean useFinderCache) {
		return getPersistence().fetchByCode(code, useFinderCache);
	}

	/**
	 * Returns the t country where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t country, or <code>null</code> if a matching t country could not be found
	 */
	public static TCountry fetchByCode(String code) {
		return getPersistence().fetchByCode(code);
	}

	/**
	 * Removes the t country where code = &#63; from the database.
	 *
	 * @param code the code
	 * @return the t country that was removed
	 */
	public static TCountry removeByCode(String code)
		throws com.aimprosoft.poc.sb.exception.NoSuchTCountryException {

		return getPersistence().removeByCode(code);
	}

	/**
	 * Returns the number of t countries where code = &#63;.
	 *
	 * @param code the code
	 * @return the number of matching t countries
	 */
	public static int countByCode(String code) {
		return getPersistence().countByCode(code);
	}

	/**
	 * Caches the t country in the entity cache if it is enabled.
	 *
	 * @param tCountry the t country
	 */
	public static void cacheResult(TCountry tCountry) {
		getPersistence().cacheResult(tCountry);
	}

	/**
	 * Caches the t countries in the entity cache if it is enabled.
	 *
	 * @param tCountries the t countries
	 */
	public static void cacheResult(List<TCountry> tCountries) {
		getPersistence().cacheResult(tCountries);
	}

	/**
	 * Creates a new t country with the primary key. Does not add the t country to the database.
	 *
	 * @param countryId the primary key for the new t country
	 * @return the new t country
	 */
	public static TCountry create(long countryId) {
		return getPersistence().create(countryId);
	}

	/**
	 * Removes the t country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country that was removed
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	public static TCountry remove(long countryId)
		throws com.aimprosoft.poc.sb.exception.NoSuchTCountryException {

		return getPersistence().remove(countryId);
	}

	public static TCountry updateImpl(TCountry tCountry) {
		return getPersistence().updateImpl(tCountry);
	}

	/**
	 * Returns the t country with the primary key or throws a <code>NoSuchTCountryException</code> if it could not be found.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country
	 * @throws NoSuchTCountryException if a t country with the primary key could not be found
	 */
	public static TCountry findByPrimaryKey(long countryId)
		throws com.aimprosoft.poc.sb.exception.NoSuchTCountryException {

		return getPersistence().findByPrimaryKey(countryId);
	}

	/**
	 * Returns the t country with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country, or <code>null</code> if a t country with the primary key could not be found
	 */
	public static TCountry fetchByPrimaryKey(long countryId) {
		return getPersistence().fetchByPrimaryKey(countryId);
	}

	/**
	 * Returns all the t countries.
	 *
	 * @return the t countries
	 */
	public static List<TCountry> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t countries
	 */
	public static List<TCountry> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of t countries
	 */
	@Deprecated
	public static List<TCountry> findAll(
		int start, int end, OrderByComparator<TCountry> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns an ordered range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t countries
	 */
	public static List<TCountry> findAll(
		int start, int end, OrderByComparator<TCountry> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Removes all the t countries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of t countries.
	 *
	 * @return the number of t countries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	/**
	 * Returns the primaryKeys of t languages associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return long[] of the primaryKeys of t languages associated with the t country
	 */
	public static long[] getTLanguagePrimaryKeys(long pk) {
		return getPersistence().getTLanguagePrimaryKeys(pk);
	}

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return the t countries associated with the t language
	 */
	public static List<TCountry> getTLanguageTCountries(long pk) {
		return getPersistence().getTLanguageTCountries(pk);
	}

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t language
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t countries associated with the t language
	 */
	public static List<TCountry> getTLanguageTCountries(
		long pk, int start, int end) {

		return getPersistence().getTLanguageTCountries(pk, start, end);
	}

	/**
	 * Returns all the t country associated with the t language.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t language
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t countries associated with the t language
	 */
	public static List<TCountry> getTLanguageTCountries(
		long pk, int start, int end,
		OrderByComparator<TCountry> orderByComparator) {

		return getPersistence().getTLanguageTCountries(
			pk, start, end, orderByComparator);
	}

	/**
	 * Returns the number of t languages associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return the number of t languages associated with the t country
	 */
	public static int getTLanguagesSize(long pk) {
		return getPersistence().getTLanguagesSize(pk);
	}

	/**
	 * Returns <code>true</code> if the t language is associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 * @return <code>true</code> if the t language is associated with the t country; <code>false</code> otherwise
	 */
	public static boolean containsTLanguage(long pk, long tLanguagePK) {
		return getPersistence().containsTLanguage(pk, tLanguagePK);
	}

	/**
	 * Returns <code>true</code> if the t country has any t languages associated with it.
	 *
	 * @param pk the primary key of the t country to check for associations with t languages
	 * @return <code>true</code> if the t country has any t languages associated with it; <code>false</code> otherwise
	 */
	public static boolean containsTLanguages(long pk) {
		return getPersistence().containsTLanguages(pk);
	}

	/**
	 * Adds an association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 */
	public static void addTLanguage(long pk, long tLanguagePK) {
		getPersistence().addTLanguage(pk, tLanguagePK);
	}

	/**
	 * Adds an association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguage the t language
	 */
	public static void addTLanguage(
		long pk, com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		getPersistence().addTLanguage(pk, tLanguage);
	}

	/**
	 * Adds an association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages
	 */
	public static void addTLanguages(long pk, long[] tLanguagePKs) {
		getPersistence().addTLanguages(pk, tLanguagePKs);
	}

	/**
	 * Adds an association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages
	 */
	public static void addTLanguages(
		long pk, List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages) {

		getPersistence().addTLanguages(pk, tLanguages);
	}

	/**
	 * Clears all associations between the t country and its t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country to clear the associated t languages from
	 */
	public static void clearTLanguages(long pk) {
		getPersistence().clearTLanguages(pk);
	}

	/**
	 * Removes the association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePK the primary key of the t language
	 */
	public static void removeTLanguage(long pk, long tLanguagePK) {
		getPersistence().removeTLanguage(pk, tLanguagePK);
	}

	/**
	 * Removes the association between the t country and the t language. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguage the t language
	 */
	public static void removeTLanguage(
		long pk, com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		getPersistence().removeTLanguage(pk, tLanguage);
	}

	/**
	 * Removes the association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages
	 */
	public static void removeTLanguages(long pk, long[] tLanguagePKs) {
		getPersistence().removeTLanguages(pk, tLanguagePKs);
	}

	/**
	 * Removes the association between the t country and the t languages. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages
	 */
	public static void removeTLanguages(
		long pk, List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages) {

		getPersistence().removeTLanguages(pk, tLanguages);
	}

	/**
	 * Sets the t languages associated with the t country, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguagePKs the primary keys of the t languages to be associated with the t country
	 */
	public static void setTLanguages(long pk, long[] tLanguagePKs) {
		getPersistence().setTLanguages(pk, tLanguagePKs);
	}

	/**
	 * Sets the t languages associated with the t country, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t country
	 * @param tLanguages the t languages to be associated with the t country
	 */
	public static void setTLanguages(
		long pk, List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages) {

		getPersistence().setTLanguages(pk, tLanguages);
	}

	public static TCountryPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TCountryPersistence, TCountryPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(TCountryPersistence.class);

		ServiceTracker<TCountryPersistence, TCountryPersistence>
			serviceTracker =
				new ServiceTracker<TCountryPersistence, TCountryPersistence>(
					bundle.getBundleContext(), TCountryPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}