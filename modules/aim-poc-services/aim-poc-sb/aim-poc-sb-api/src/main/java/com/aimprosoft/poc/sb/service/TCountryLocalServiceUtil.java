/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for TCountry. This utility wraps
 * <code>com.aimprosoft.poc.sb.service.impl.TCountryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see TCountryLocalService
 * @generated
 */
@ProviderType
public class TCountryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.aimprosoft.poc.sb.service.impl.TCountryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.aimprosoft.poc.sb.model.TCountry addTCountry(
		long companyId, String code, String name, String capital,
		String phoneCode, String continentCode, String currency, String flag,
		java.util.Map<String, String> languageMap) {

		return getService().addTCountry(
			companyId, code, name, capital, phoneCode, continentCode, currency,
			flag, languageMap);
	}

	/**
	 * Adds the t country to the database. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was added
	 */
	public static com.aimprosoft.poc.sb.model.TCountry addTCountry(
		com.aimprosoft.poc.sb.model.TCountry tCountry) {

		return getService().addTCountry(tCountry);
	}

	public static void addTLanguageTCountries(
		long languageId,
		java.util.List<com.aimprosoft.poc.sb.model.TCountry> tCountries) {

		getService().addTLanguageTCountries(languageId, tCountries);
	}

	public static void addTLanguageTCountries(
		long languageId, long[] countryIds) {

		getService().addTLanguageTCountries(languageId, countryIds);
	}

	public static void addTLanguageTCountry(long languageId, long countryId) {
		getService().addTLanguageTCountry(languageId, countryId);
	}

	public static void addTLanguageTCountry(
		long languageId, com.aimprosoft.poc.sb.model.TCountry tCountry) {

		getService().addTLanguageTCountry(languageId, tCountry);
	}

	public static void clearTLanguageTCountries(long languageId) {
		getService().clearTLanguageTCountries(languageId);
	}

	public static boolean countryExists(String countryCode) {
		return getService().countryExists(countryCode);
	}

	/**
	 * Creates a new t country with the primary key. Does not add the t country to the database.
	 *
	 * @param countryId the primary key for the new t country
	 * @return the new t country
	 */
	public static com.aimprosoft.poc.sb.model.TCountry createTCountry(
		long countryId) {

		return getService().createTCountry(countryId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the t country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country that was removed
	 * @throws PortalException if a t country with the primary key could not be found
	 */
	public static com.aimprosoft.poc.sb.model.TCountry deleteTCountry(
			long countryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteTCountry(countryId);
	}

	/**
	 * Deletes the t country from the database. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was removed
	 */
	public static com.aimprosoft.poc.sb.model.TCountry deleteTCountry(
		com.aimprosoft.poc.sb.model.TCountry tCountry) {

		return getService().deleteTCountry(tCountry);
	}

	public static void deleteTLanguageTCountries(
		long languageId,
		java.util.List<com.aimprosoft.poc.sb.model.TCountry> tCountries) {

		getService().deleteTLanguageTCountries(languageId, tCountries);
	}

	public static void deleteTLanguageTCountries(
		long languageId, long[] countryIds) {

		getService().deleteTLanguageTCountries(languageId, countryIds);
	}

	public static void deleteTLanguageTCountry(
		long languageId, long countryId) {

		getService().deleteTLanguageTCountry(languageId, countryId);
	}

	public static void deleteTLanguageTCountry(
		long languageId, com.aimprosoft.poc.sb.model.TCountry tCountry) {

		getService().deleteTLanguageTCountry(languageId, tCountry);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.aimprosoft.poc.sb.model.TCountry fetchTCountry(
		long countryId) {

		return getService().fetchTCountry(countryId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.aimprosoft.poc.sb.model.TCountry getCountryByCode(
		String countryCode) {

		return getService().getCountryByCode(countryCode);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t countries
	 */
	public static java.util.List<com.aimprosoft.poc.sb.model.TCountry>
		getTCountries(int start, int end) {

		return getService().getTCountries(start, end);
	}

	/**
	 * Returns the number of t countries.
	 *
	 * @return the number of t countries
	 */
	public static int getTCountriesCount() {
		return getService().getTCountriesCount();
	}

	/**
	 * Returns the t country with the primary key.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country
	 * @throws PortalException if a t country with the primary key could not be found
	 */
	public static com.aimprosoft.poc.sb.model.TCountry getTCountry(
			long countryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getTCountry(countryId);
	}

	/**
	 * Returns the languageIds of the t languages associated with the t country.
	 *
	 * @param countryId the countryId of the t country
	 * @return long[] the languageIds of t languages associated with the t country
	 */
	public static long[] getTLanguagePrimaryKeys(long countryId) {
		return getService().getTLanguagePrimaryKeys(countryId);
	}

	public static java.util.List<com.aimprosoft.poc.sb.model.TCountry>
		getTLanguageTCountries(long languageId) {

		return getService().getTLanguageTCountries(languageId);
	}

	public static java.util.List<com.aimprosoft.poc.sb.model.TCountry>
		getTLanguageTCountries(long languageId, int start, int end) {

		return getService().getTLanguageTCountries(languageId, start, end);
	}

	public static java.util.List<com.aimprosoft.poc.sb.model.TCountry>
		getTLanguageTCountries(
			long languageId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.aimprosoft.poc.sb.model.TCountry> orderByComparator) {

		return getService().getTLanguageTCountries(
			languageId, start, end, orderByComparator);
	}

	public static int getTLanguageTCountriesCount(long languageId) {
		return getService().getTLanguageTCountriesCount(languageId);
	}

	public static boolean hasTLanguageTCountries(long languageId) {
		return getService().hasTLanguageTCountries(languageId);
	}

	public static boolean hasTLanguageTCountry(
		long languageId, long countryId) {

		return getService().hasTLanguageTCountry(languageId, countryId);
	}

	public static void setTLanguageTCountries(
		long languageId, long[] countryIds) {

		getService().setTLanguageTCountries(languageId, countryIds);
	}

	/**
	 * Updates the t country in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was updated
	 */
	public static com.aimprosoft.poc.sb.model.TCountry updateTCountry(
		com.aimprosoft.poc.sb.model.TCountry tCountry) {

		return getService().updateTCountry(tCountry);
	}

	public static TCountryLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TCountryLocalService, TCountryLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(TCountryLocalService.class);

		ServiceTracker<TCountryLocalService, TCountryLocalService>
			serviceTracker =
				new ServiceTracker<TCountryLocalService, TCountryLocalService>(
					bundle.getBundleContext(), TCountryLocalService.class,
					null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}