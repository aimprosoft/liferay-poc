/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link TLanguage}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TLanguage
 * @generated
 */
@ProviderType
public class TLanguageWrapper
	extends BaseModelWrapper<TLanguage>
	implements TLanguage, ModelWrapper<TLanguage> {

	public TLanguageWrapper(TLanguage tLanguage) {
		super(tLanguage);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("languageId", getLanguageId());
		attributes.put("companyId", getCompanyId());
		attributes.put("code", getCode());
		attributes.put("name", getName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long languageId = (Long)attributes.get("languageId");

		if (languageId != null) {
			setLanguageId(languageId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String code = (String)attributes.get("code");

		if (code != null) {
			setCode(code);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}
	}

	/**
	 * Returns the code of this t language.
	 *
	 * @return the code of this t language
	 */
	@Override
	public String getCode() {
		return model.getCode();
	}

	/**
	 * Returns the company ID of this t language.
	 *
	 * @return the company ID of this t language
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the language ID of this t language.
	 *
	 * @return the language ID of this t language
	 */
	@Override
	public long getLanguageId() {
		return model.getLanguageId();
	}

	/**
	 * Returns the name of this t language.
	 *
	 * @return the name of this t language
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the primary key of this t language.
	 *
	 * @return the primary key of this t language
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the code of this t language.
	 *
	 * @param code the code of this t language
	 */
	@Override
	public void setCode(String code) {
		model.setCode(code);
	}

	/**
	 * Sets the company ID of this t language.
	 *
	 * @param companyId the company ID of this t language
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the language ID of this t language.
	 *
	 * @param languageId the language ID of this t language
	 */
	@Override
	public void setLanguageId(long languageId) {
		model.setLanguageId(languageId);
	}

	/**
	 * Sets the name of this t language.
	 *
	 * @param name the name of this t language
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the primary key of this t language.
	 *
	 * @param primaryKey the primary key of this t language
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	@Override
	protected TLanguageWrapper wrap(TLanguage tLanguage) {
		return new TLanguageWrapper(tLanguage);
	}

}