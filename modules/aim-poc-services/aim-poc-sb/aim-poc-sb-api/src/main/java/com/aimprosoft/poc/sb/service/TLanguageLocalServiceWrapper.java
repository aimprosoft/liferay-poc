/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a wrapper for {@link TLanguageLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see TLanguageLocalService
 * @generated
 */
@ProviderType
public class TLanguageLocalServiceWrapper
	implements TLanguageLocalService, ServiceWrapper<TLanguageLocalService> {

	public TLanguageLocalServiceWrapper(
		TLanguageLocalService tLanguageLocalService) {

		_tLanguageLocalService = tLanguageLocalService;
	}

	@Override
	public void addTCountryTLanguage(long countryId, long languageId) {
		_tLanguageLocalService.addTCountryTLanguage(countryId, languageId);
	}

	@Override
	public void addTCountryTLanguage(
		long countryId, com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		_tLanguageLocalService.addTCountryTLanguage(countryId, tLanguage);
	}

	@Override
	public void addTCountryTLanguages(
		long countryId,
		java.util.List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages) {

		_tLanguageLocalService.addTCountryTLanguages(countryId, tLanguages);
	}

	@Override
	public void addTCountryTLanguages(long countryId, long[] languageIds) {
		_tLanguageLocalService.addTCountryTLanguages(countryId, languageIds);
	}

	/**
	 * Adds the t language to the database. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was added
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TLanguage addTLanguage(
		com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		return _tLanguageLocalService.addTLanguage(tLanguage);
	}

	@Override
	public void clearTCountryTLanguages(long countryId) {
		_tLanguageLocalService.clearTCountryTLanguages(countryId);
	}

	/**
	 * Creates a new t language with the primary key. Does not add the t language to the database.
	 *
	 * @param languageId the primary key for the new t language
	 * @return the new t language
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TLanguage createTLanguage(
		long languageId) {

		return _tLanguageLocalService.createTLanguage(languageId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tLanguageLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public void deleteTCountryTLanguage(long countryId, long languageId) {
		_tLanguageLocalService.deleteTCountryTLanguage(countryId, languageId);
	}

	@Override
	public void deleteTCountryTLanguage(
		long countryId, com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		_tLanguageLocalService.deleteTCountryTLanguage(countryId, tLanguage);
	}

	@Override
	public void deleteTCountryTLanguages(
		long countryId,
		java.util.List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages) {

		_tLanguageLocalService.deleteTCountryTLanguages(countryId, tLanguages);
	}

	@Override
	public void deleteTCountryTLanguages(long countryId, long[] languageIds) {
		_tLanguageLocalService.deleteTCountryTLanguages(countryId, languageIds);
	}

	/**
	 * Deletes the t language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language that was removed
	 * @throws PortalException if a t language with the primary key could not be found
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TLanguage deleteTLanguage(
			long languageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tLanguageLocalService.deleteTLanguage(languageId);
	}

	/**
	 * Deletes the t language from the database. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was removed
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TLanguage deleteTLanguage(
		com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		return _tLanguageLocalService.deleteTLanguage(tLanguage);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _tLanguageLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _tLanguageLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _tLanguageLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _tLanguageLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _tLanguageLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _tLanguageLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.aimprosoft.poc.sb.model.TLanguage fetchTLanguage(
		long languageId) {

		return _tLanguageLocalService.fetchTLanguage(languageId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _tLanguageLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _tLanguageLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public com.aimprosoft.poc.sb.model.TLanguage getLanguageByCode(
		String languageCode) {

		return _tLanguageLocalService.getLanguageByCode(languageCode);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _tLanguageLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tLanguageLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the countryIds of the t countries associated with the t language.
	 *
	 * @param languageId the languageId of the t language
	 * @return long[] the countryIds of t countries associated with the t language
	 */
	@Override
	public long[] getTCountryPrimaryKeys(long languageId) {
		return _tLanguageLocalService.getTCountryPrimaryKeys(languageId);
	}

	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TLanguage>
		getTCountryTLanguages(long countryId) {

		return _tLanguageLocalService.getTCountryTLanguages(countryId);
	}

	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TLanguage>
		getTCountryTLanguages(long countryId, int start, int end) {

		return _tLanguageLocalService.getTCountryTLanguages(
			countryId, start, end);
	}

	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TLanguage>
		getTCountryTLanguages(
			long countryId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.aimprosoft.poc.sb.model.TLanguage> orderByComparator) {

		return _tLanguageLocalService.getTCountryTLanguages(
			countryId, start, end, orderByComparator);
	}

	@Override
	public int getTCountryTLanguagesCount(long countryId) {
		return _tLanguageLocalService.getTCountryTLanguagesCount(countryId);
	}

	/**
	 * Returns the t language with the primary key.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language
	 * @throws PortalException if a t language with the primary key could not be found
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TLanguage getTLanguage(long languageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tLanguageLocalService.getTLanguage(languageId);
	}

	/**
	 * Returns a range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t languages
	 */
	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TLanguage> getTLanguages(
		int start, int end) {

		return _tLanguageLocalService.getTLanguages(start, end);
	}

	/**
	 * Returns the number of t languages.
	 *
	 * @return the number of t languages
	 */
	@Override
	public int getTLanguagesCount() {
		return _tLanguageLocalService.getTLanguagesCount();
	}

	@Override
	public boolean hasTCountryTLanguage(long countryId, long languageId) {
		return _tLanguageLocalService.hasTCountryTLanguage(
			countryId, languageId);
	}

	@Override
	public boolean hasTCountryTLanguages(long countryId) {
		return _tLanguageLocalService.hasTCountryTLanguages(countryId);
	}

	@Override
	public void setTCountryTLanguages(long countryId, long[] languageIds) {
		_tLanguageLocalService.setTCountryTLanguages(countryId, languageIds);
	}

	/**
	 * Updates the t language in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was updated
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TLanguage updateTLanguage(
		com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		return _tLanguageLocalService.updateTLanguage(tLanguage);
	}

	@Override
	public TLanguageLocalService getWrappedService() {
		return _tLanguageLocalService;
	}

	@Override
	public void setWrappedService(TLanguageLocalService tLanguageLocalService) {
		_tLanguageLocalService = tLanguageLocalService;
	}

	private TLanguageLocalService _tLanguageLocalService;

}