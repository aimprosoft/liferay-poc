/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services, specifically {@link com.aimprosoft.poc.sb.service.http.TCountryServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class TCountrySoap implements Serializable {

	public static TCountrySoap toSoapModel(TCountry model) {
		TCountrySoap soapModel = new TCountrySoap();

		soapModel.setCountryId(model.getCountryId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCode(model.getCode());
		soapModel.setName(model.getName());
		soapModel.setCapital(model.getCapital());
		soapModel.setPhoneCode(model.getPhoneCode());
		soapModel.setContinentCode(model.getContinentCode());
		soapModel.setCurrency(model.getCurrency());
		soapModel.setFlag(model.getFlag());

		return soapModel;
	}

	public static TCountrySoap[] toSoapModels(TCountry[] models) {
		TCountrySoap[] soapModels = new TCountrySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TCountrySoap[][] toSoapModels(TCountry[][] models) {
		TCountrySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TCountrySoap[models.length][models[0].length];
		}
		else {
			soapModels = new TCountrySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TCountrySoap[] toSoapModels(List<TCountry> models) {
		List<TCountrySoap> soapModels = new ArrayList<TCountrySoap>(
			models.size());

		for (TCountry model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TCountrySoap[soapModels.size()]);
	}

	public TCountrySoap() {
	}

	public long getPrimaryKey() {
		return _countryId;
	}

	public void setPrimaryKey(long pk) {
		setCountryId(pk);
	}

	public long getCountryId() {
		return _countryId;
	}

	public void setCountryId(long countryId) {
		_countryId = countryId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getCode() {
		return _code;
	}

	public void setCode(String code) {
		_code = code;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getCapital() {
		return _capital;
	}

	public void setCapital(String capital) {
		_capital = capital;
	}

	public String getPhoneCode() {
		return _phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		_phoneCode = phoneCode;
	}

	public String getContinentCode() {
		return _continentCode;
	}

	public void setContinentCode(String continentCode) {
		_continentCode = continentCode;
	}

	public String getCurrency() {
		return _currency;
	}

	public void setCurrency(String currency) {
		_currency = currency;
	}

	public String getFlag() {
		return _flag;
	}

	public void setFlag(String flag) {
		_flag = flag;
	}

	private long _countryId;
	private long _companyId;
	private String _code;
	private String _name;
	private String _capital;
	private String _phoneCode;
	private String _continentCode;
	private String _currency;
	private String _flag;

}