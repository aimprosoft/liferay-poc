/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * This class is used by SOAP remote services, specifically {@link com.aimprosoft.poc.sb.service.http.TLanguageServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class TLanguageSoap implements Serializable {

	public static TLanguageSoap toSoapModel(TLanguage model) {
		TLanguageSoap soapModel = new TLanguageSoap();

		soapModel.setLanguageId(model.getLanguageId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCode(model.getCode());
		soapModel.setName(model.getName());

		return soapModel;
	}

	public static TLanguageSoap[] toSoapModels(TLanguage[] models) {
		TLanguageSoap[] soapModels = new TLanguageSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TLanguageSoap[][] toSoapModels(TLanguage[][] models) {
		TLanguageSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TLanguageSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TLanguageSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TLanguageSoap[] toSoapModels(List<TLanguage> models) {
		List<TLanguageSoap> soapModels = new ArrayList<TLanguageSoap>(
			models.size());

		for (TLanguage model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TLanguageSoap[soapModels.size()]);
	}

	public TLanguageSoap() {
	}

	public long getPrimaryKey() {
		return _languageId;
	}

	public void setPrimaryKey(long pk) {
		setLanguageId(pk);
	}

	public long getLanguageId() {
		return _languageId;
	}

	public void setLanguageId(long languageId) {
		_languageId = languageId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public String getCode() {
		return _code;
	}

	public void setCode(String code) {
		_code = code;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	private long _languageId;
	private long _companyId;
	private String _code;
	private String _name;

}