/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service;

import com.aimprosoft.poc.sb.model.TCountry;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for TCountry. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see TCountryLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface TCountryLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TCountryLocalServiceUtil} to access the t country local service. Add custom service methods to <code>com.aimprosoft.poc.sb.service.impl.TCountryLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public TCountry addTCountry(
		long companyId, String code, String name, String capital,
		String phoneCode, String continentCode, String currency, String flag,
		Map<String, String> languageMap);

	/**
	 * Adds the t country to the database. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public TCountry addTCountry(TCountry tCountry);

	public void addTLanguageTCountries(
		long languageId, List<TCountry> tCountries);

	public void addTLanguageTCountries(long languageId, long[] countryIds);

	public void addTLanguageTCountry(long languageId, long countryId);

	public void addTLanguageTCountry(long languageId, TCountry tCountry);

	public void clearTLanguageTCountries(long languageId);

	public boolean countryExists(String countryCode);

	/**
	 * Creates a new t country with the primary key. Does not add the t country to the database.
	 *
	 * @param countryId the primary key for the new t country
	 * @return the new t country
	 */
	@Transactional(enabled = false)
	public TCountry createTCountry(long countryId);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the t country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country that was removed
	 * @throws PortalException if a t country with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public TCountry deleteTCountry(long countryId) throws PortalException;

	/**
	 * Deletes the t country from the database. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public TCountry deleteTCountry(TCountry tCountry);

	public void deleteTLanguageTCountries(
		long languageId, List<TCountry> tCountries);

	public void deleteTLanguageTCountries(long languageId, long[] countryIds);

	public void deleteTLanguageTCountry(long languageId, long countryId);

	public void deleteTLanguageTCountry(long languageId, TCountry tCountry);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TCountry fetchTCountry(long countryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TCountry getCountryByCode(String countryCode);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns a range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t countries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TCountry> getTCountries(int start, int end);

	/**
	 * Returns the number of t countries.
	 *
	 * @return the number of t countries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTCountriesCount();

	/**
	 * Returns the t country with the primary key.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country
	 * @throws PortalException if a t country with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TCountry getTCountry(long countryId) throws PortalException;

	/**
	 * Returns the languageIds of the t languages associated with the t country.
	 *
	 * @param countryId the countryId of the t country
	 * @return long[] the languageIds of t languages associated with the t country
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getTLanguagePrimaryKeys(long countryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TCountry> getTLanguageTCountries(long languageId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TCountry> getTLanguageTCountries(
		long languageId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TCountry> getTLanguageTCountries(
		long languageId, int start, int end,
		OrderByComparator<TCountry> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTLanguageTCountriesCount(long languageId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasTLanguageTCountries(long languageId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasTLanguageTCountry(long languageId, long countryId);

	public void setTLanguageTCountries(long languageId, long[] countryIds);

	/**
	 * Updates the t country in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public TCountry updateTCountry(TCountry tCountry);

}