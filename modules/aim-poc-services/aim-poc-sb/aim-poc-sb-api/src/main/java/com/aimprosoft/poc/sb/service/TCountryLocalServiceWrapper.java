/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a wrapper for {@link TCountryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see TCountryLocalService
 * @generated
 */
@ProviderType
public class TCountryLocalServiceWrapper
	implements TCountryLocalService, ServiceWrapper<TCountryLocalService> {

	public TCountryLocalServiceWrapper(
		TCountryLocalService tCountryLocalService) {

		_tCountryLocalService = tCountryLocalService;
	}

	@Override
	public com.aimprosoft.poc.sb.model.TCountry addTCountry(
		long companyId, String code, String name, String capital,
		String phoneCode, String continentCode, String currency, String flag,
		java.util.Map<String, String> languageMap) {

		return _tCountryLocalService.addTCountry(
			companyId, code, name, capital, phoneCode, continentCode, currency,
			flag, languageMap);
	}

	/**
	 * Adds the t country to the database. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was added
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TCountry addTCountry(
		com.aimprosoft.poc.sb.model.TCountry tCountry) {

		return _tCountryLocalService.addTCountry(tCountry);
	}

	@Override
	public void addTLanguageTCountries(
		long languageId,
		java.util.List<com.aimprosoft.poc.sb.model.TCountry> tCountries) {

		_tCountryLocalService.addTLanguageTCountries(languageId, tCountries);
	}

	@Override
	public void addTLanguageTCountries(long languageId, long[] countryIds) {
		_tCountryLocalService.addTLanguageTCountries(languageId, countryIds);
	}

	@Override
	public void addTLanguageTCountry(long languageId, long countryId) {
		_tCountryLocalService.addTLanguageTCountry(languageId, countryId);
	}

	@Override
	public void addTLanguageTCountry(
		long languageId, com.aimprosoft.poc.sb.model.TCountry tCountry) {

		_tCountryLocalService.addTLanguageTCountry(languageId, tCountry);
	}

	@Override
	public void clearTLanguageTCountries(long languageId) {
		_tCountryLocalService.clearTLanguageTCountries(languageId);
	}

	@Override
	public boolean countryExists(String countryCode) {
		return _tCountryLocalService.countryExists(countryCode);
	}

	/**
	 * Creates a new t country with the primary key. Does not add the t country to the database.
	 *
	 * @param countryId the primary key for the new t country
	 * @return the new t country
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TCountry createTCountry(long countryId) {
		return _tCountryLocalService.createTCountry(countryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tCountryLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the t country with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country that was removed
	 * @throws PortalException if a t country with the primary key could not be found
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TCountry deleteTCountry(long countryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tCountryLocalService.deleteTCountry(countryId);
	}

	/**
	 * Deletes the t country from the database. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was removed
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TCountry deleteTCountry(
		com.aimprosoft.poc.sb.model.TCountry tCountry) {

		return _tCountryLocalService.deleteTCountry(tCountry);
	}

	@Override
	public void deleteTLanguageTCountries(
		long languageId,
		java.util.List<com.aimprosoft.poc.sb.model.TCountry> tCountries) {

		_tCountryLocalService.deleteTLanguageTCountries(languageId, tCountries);
	}

	@Override
	public void deleteTLanguageTCountries(long languageId, long[] countryIds) {
		_tCountryLocalService.deleteTLanguageTCountries(languageId, countryIds);
	}

	@Override
	public void deleteTLanguageTCountry(long languageId, long countryId) {
		_tCountryLocalService.deleteTLanguageTCountry(languageId, countryId);
	}

	@Override
	public void deleteTLanguageTCountry(
		long languageId, com.aimprosoft.poc.sb.model.TCountry tCountry) {

		_tCountryLocalService.deleteTLanguageTCountry(languageId, tCountry);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _tCountryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _tCountryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _tCountryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _tCountryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _tCountryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _tCountryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.aimprosoft.poc.sb.model.TCountry fetchTCountry(long countryId) {
		return _tCountryLocalService.fetchTCountry(countryId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _tCountryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.aimprosoft.poc.sb.model.TCountry getCountryByCode(
		String countryCode) {

		return _tCountryLocalService.getCountryByCode(countryCode);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _tCountryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _tCountryLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tCountryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the t countries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TCountryModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t countries
	 */
	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TCountry> getTCountries(
		int start, int end) {

		return _tCountryLocalService.getTCountries(start, end);
	}

	/**
	 * Returns the number of t countries.
	 *
	 * @return the number of t countries
	 */
	@Override
	public int getTCountriesCount() {
		return _tCountryLocalService.getTCountriesCount();
	}

	/**
	 * Returns the t country with the primary key.
	 *
	 * @param countryId the primary key of the t country
	 * @return the t country
	 * @throws PortalException if a t country with the primary key could not be found
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TCountry getTCountry(long countryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _tCountryLocalService.getTCountry(countryId);
	}

	/**
	 * Returns the languageIds of the t languages associated with the t country.
	 *
	 * @param countryId the countryId of the t country
	 * @return long[] the languageIds of t languages associated with the t country
	 */
	@Override
	public long[] getTLanguagePrimaryKeys(long countryId) {
		return _tCountryLocalService.getTLanguagePrimaryKeys(countryId);
	}

	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TCountry>
		getTLanguageTCountries(long languageId) {

		return _tCountryLocalService.getTLanguageTCountries(languageId);
	}

	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TCountry>
		getTLanguageTCountries(long languageId, int start, int end) {

		return _tCountryLocalService.getTLanguageTCountries(
			languageId, start, end);
	}

	@Override
	public java.util.List<com.aimprosoft.poc.sb.model.TCountry>
		getTLanguageTCountries(
			long languageId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.aimprosoft.poc.sb.model.TCountry> orderByComparator) {

		return _tCountryLocalService.getTLanguageTCountries(
			languageId, start, end, orderByComparator);
	}

	@Override
	public int getTLanguageTCountriesCount(long languageId) {
		return _tCountryLocalService.getTLanguageTCountriesCount(languageId);
	}

	@Override
	public boolean hasTLanguageTCountries(long languageId) {
		return _tCountryLocalService.hasTLanguageTCountries(languageId);
	}

	@Override
	public boolean hasTLanguageTCountry(long languageId, long countryId) {
		return _tCountryLocalService.hasTLanguageTCountry(
			languageId, countryId);
	}

	@Override
	public void setTLanguageTCountries(long languageId, long[] countryIds) {
		_tCountryLocalService.setTLanguageTCountries(languageId, countryIds);
	}

	/**
	 * Updates the t country in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param tCountry the t country
	 * @return the t country that was updated
	 */
	@Override
	public com.aimprosoft.poc.sb.model.TCountry updateTCountry(
		com.aimprosoft.poc.sb.model.TCountry tCountry) {

		return _tCountryLocalService.updateTCountry(tCountry);
	}

	@Override
	public TCountryLocalService getWrappedService() {
		return _tCountryLocalService;
	}

	@Override
	public void setWrappedService(TCountryLocalService tCountryLocalService) {
		_tCountryLocalService = tCountryLocalService;
	}

	private TCountryLocalService _tCountryLocalService;

}