/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service;

import com.aimprosoft.poc.sb.model.TLanguage;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for TLanguage. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see TLanguageLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface TLanguageLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TLanguageLocalServiceUtil} to access the t language local service. Add custom service methods to <code>com.aimprosoft.poc.sb.service.impl.TLanguageLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public void addTCountryTLanguage(long countryId, long languageId);

	public void addTCountryTLanguage(long countryId, TLanguage tLanguage);

	public void addTCountryTLanguages(
		long countryId, List<TLanguage> tLanguages);

	public void addTCountryTLanguages(long countryId, long[] languageIds);

	/**
	 * Adds the t language to the database. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public TLanguage addTLanguage(TLanguage tLanguage);

	public void clearTCountryTLanguages(long countryId);

	/**
	 * Creates a new t language with the primary key. Does not add the t language to the database.
	 *
	 * @param languageId the primary key for the new t language
	 * @return the new t language
	 */
	@Transactional(enabled = false)
	public TLanguage createTLanguage(long languageId);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	public void deleteTCountryTLanguage(long countryId, long languageId);

	public void deleteTCountryTLanguage(long countryId, TLanguage tLanguage);

	public void deleteTCountryTLanguages(
		long countryId, List<TLanguage> tLanguages);

	public void deleteTCountryTLanguages(long countryId, long[] languageIds);

	/**
	 * Deletes the t language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language that was removed
	 * @throws PortalException if a t language with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public TLanguage deleteTLanguage(long languageId) throws PortalException;

	/**
	 * Deletes the t language from the database. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public TLanguage deleteTLanguage(TLanguage tLanguage);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TLanguage fetchTLanguage(long languageId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TLanguage getLanguageByCode(String languageCode);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns the countryIds of the t countries associated with the t language.
	 *
	 * @param languageId the languageId of the t language
	 * @return long[] the countryIds of t countries associated with the t language
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long[] getTCountryPrimaryKeys(long languageId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TLanguage> getTCountryTLanguages(long countryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TLanguage> getTCountryTLanguages(
		long countryId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TLanguage> getTCountryTLanguages(
		long countryId, int start, int end,
		OrderByComparator<TLanguage> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTCountryTLanguagesCount(long countryId);

	/**
	 * Returns the t language with the primary key.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language
	 * @throws PortalException if a t language with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public TLanguage getTLanguage(long languageId) throws PortalException;

	/**
	 * Returns a range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t languages
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TLanguage> getTLanguages(int start, int end);

	/**
	 * Returns the number of t languages.
	 *
	 * @return the number of t languages
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTLanguagesCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasTCountryTLanguage(long countryId, long languageId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean hasTCountryTLanguages(long countryId);

	public void setTCountryTLanguages(long countryId, long[] languageIds);

	/**
	 * Updates the t language in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public TLanguage updateTLanguage(TLanguage tLanguage);

}