/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * <p>
 * This class is a wrapper for {@link TCountry}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TCountry
 * @generated
 */
@ProviderType
public class TCountryWrapper
	extends BaseModelWrapper<TCountry>
	implements TCountry, ModelWrapper<TCountry> {

	public TCountryWrapper(TCountry tCountry) {
		super(tCountry);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("countryId", getCountryId());
		attributes.put("companyId", getCompanyId());
		attributes.put("code", getCode());
		attributes.put("name", getName());
		attributes.put("capital", getCapital());
		attributes.put("phoneCode", getPhoneCode());
		attributes.put("continentCode", getContinentCode());
		attributes.put("currency", getCurrency());
		attributes.put("flag", getFlag());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long countryId = (Long)attributes.get("countryId");

		if (countryId != null) {
			setCountryId(countryId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		String code = (String)attributes.get("code");

		if (code != null) {
			setCode(code);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String capital = (String)attributes.get("capital");

		if (capital != null) {
			setCapital(capital);
		}

		String phoneCode = (String)attributes.get("phoneCode");

		if (phoneCode != null) {
			setPhoneCode(phoneCode);
		}

		String continentCode = (String)attributes.get("continentCode");

		if (continentCode != null) {
			setContinentCode(continentCode);
		}

		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		String flag = (String)attributes.get("flag");

		if (flag != null) {
			setFlag(flag);
		}
	}

	/**
	 * Returns the capital of this t country.
	 *
	 * @return the capital of this t country
	 */
	@Override
	public String getCapital() {
		return model.getCapital();
	}

	/**
	 * Returns the code of this t country.
	 *
	 * @return the code of this t country
	 */
	@Override
	public String getCode() {
		return model.getCode();
	}

	/**
	 * Returns the company ID of this t country.
	 *
	 * @return the company ID of this t country
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the continent code of this t country.
	 *
	 * @return the continent code of this t country
	 */
	@Override
	public String getContinentCode() {
		return model.getContinentCode();
	}

	/**
	 * Returns the country ID of this t country.
	 *
	 * @return the country ID of this t country
	 */
	@Override
	public long getCountryId() {
		return model.getCountryId();
	}

	/**
	 * Returns the currency of this t country.
	 *
	 * @return the currency of this t country
	 */
	@Override
	public String getCurrency() {
		return model.getCurrency();
	}

	/**
	 * Returns the flag of this t country.
	 *
	 * @return the flag of this t country
	 */
	@Override
	public String getFlag() {
		return model.getFlag();
	}

	@Override
	public java.util.List<TLanguage> getLanguages() {
		return model.getLanguages();
	}

	/**
	 * Returns the name of this t country.
	 *
	 * @return the name of this t country
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the phone code of this t country.
	 *
	 * @return the phone code of this t country
	 */
	@Override
	public String getPhoneCode() {
		return model.getPhoneCode();
	}

	/**
	 * Returns the primary key of this t country.
	 *
	 * @return the primary key of this t country
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the capital of this t country.
	 *
	 * @param capital the capital of this t country
	 */
	@Override
	public void setCapital(String capital) {
		model.setCapital(capital);
	}

	/**
	 * Sets the code of this t country.
	 *
	 * @param code the code of this t country
	 */
	@Override
	public void setCode(String code) {
		model.setCode(code);
	}

	/**
	 * Sets the company ID of this t country.
	 *
	 * @param companyId the company ID of this t country
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the continent code of this t country.
	 *
	 * @param continentCode the continent code of this t country
	 */
	@Override
	public void setContinentCode(String continentCode) {
		model.setContinentCode(continentCode);
	}

	/**
	 * Sets the country ID of this t country.
	 *
	 * @param countryId the country ID of this t country
	 */
	@Override
	public void setCountryId(long countryId) {
		model.setCountryId(countryId);
	}

	/**
	 * Sets the currency of this t country.
	 *
	 * @param currency the currency of this t country
	 */
	@Override
	public void setCurrency(String currency) {
		model.setCurrency(currency);
	}

	/**
	 * Sets the flag of this t country.
	 *
	 * @param flag the flag of this t country
	 */
	@Override
	public void setFlag(String flag) {
		model.setFlag(flag);
	}

	/**
	 * Sets the name of this t country.
	 *
	 * @param name the name of this t country
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the phone code of this t country.
	 *
	 * @param phoneCode the phone code of this t country
	 */
	@Override
	public void setPhoneCode(String phoneCode) {
		model.setPhoneCode(phoneCode);
	}

	/**
	 * Sets the primary key of this t country.
	 *
	 * @param primaryKey the primary key of this t country
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	@Override
	protected TCountryWrapper wrap(TCountry tCountry) {
		return new TCountryWrapper(tCountry);
	}

}