/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service.persistence;

import com.aimprosoft.poc.sb.exception.NoSuchTLanguageException;
import com.aimprosoft.poc.sb.model.TLanguage;

import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the t language service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TLanguageUtil
 * @generated
 */
@ProviderType
public interface TLanguagePersistence extends BasePersistence<TLanguage> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TLanguageUtil} to access the t language persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns the t language where code = &#63; or throws a <code>NoSuchTLanguageException</code> if it could not be found.
	 *
	 * @param code the code
	 * @return the matching t language
	 * @throws NoSuchTLanguageException if a matching t language could not be found
	 */
	public TLanguage findByCode(String code) throws NoSuchTLanguageException;

	/**
	 * Returns the t language where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #fetchByCode(String)}
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t language, or <code>null</code> if a matching t language could not be found
	 */
	@Deprecated
	public TLanguage fetchByCode(String code, boolean useFinderCache);

	/**
	 * Returns the t language where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param code the code
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching t language, or <code>null</code> if a matching t language could not be found
	 */
	public TLanguage fetchByCode(String code);

	/**
	 * Removes the t language where code = &#63; from the database.
	 *
	 * @param code the code
	 * @return the t language that was removed
	 */
	public TLanguage removeByCode(String code) throws NoSuchTLanguageException;

	/**
	 * Returns the number of t languages where code = &#63;.
	 *
	 * @param code the code
	 * @return the number of matching t languages
	 */
	public int countByCode(String code);

	/**
	 * Caches the t language in the entity cache if it is enabled.
	 *
	 * @param tLanguage the t language
	 */
	public void cacheResult(TLanguage tLanguage);

	/**
	 * Caches the t languages in the entity cache if it is enabled.
	 *
	 * @param tLanguages the t languages
	 */
	public void cacheResult(java.util.List<TLanguage> tLanguages);

	/**
	 * Creates a new t language with the primary key. Does not add the t language to the database.
	 *
	 * @param languageId the primary key for the new t language
	 * @return the new t language
	 */
	public TLanguage create(long languageId);

	/**
	 * Removes the t language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language that was removed
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	public TLanguage remove(long languageId) throws NoSuchTLanguageException;

	public TLanguage updateImpl(TLanguage tLanguage);

	/**
	 * Returns the t language with the primary key or throws a <code>NoSuchTLanguageException</code> if it could not be found.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language
	 * @throws NoSuchTLanguageException if a t language with the primary key could not be found
	 */
	public TLanguage findByPrimaryKey(long languageId)
		throws NoSuchTLanguageException;

	/**
	 * Returns the t language with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language, or <code>null</code> if a t language with the primary key could not be found
	 */
	public TLanguage fetchByPrimaryKey(long languageId);

	/**
	 * Returns all the t languages.
	 *
	 * @return the t languages
	 */
	public java.util.List<TLanguage> findAll();

	/**
	 * Returns a range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t languages
	 */
	public java.util.List<TLanguage> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @deprecated As of Mueller (7.2.x), replaced by {@link #findAll(int, int, OrderByComparator)}
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of t languages
	 */
	@Deprecated
	public java.util.List<TLanguage> findAll(
		int start, int end, OrderByComparator<TLanguage> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns an ordered range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t languages
	 */
	public java.util.List<TLanguage> findAll(
		int start, int end, OrderByComparator<TLanguage> orderByComparator);

	/**
	 * Removes all the t languages from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of t languages.
	 *
	 * @return the number of t languages
	 */
	public int countAll();

	/**
	 * Returns the primaryKeys of t countries associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return long[] of the primaryKeys of t countries associated with the t language
	 */
	public long[] getTCountryPrimaryKeys(long pk);

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * @param pk the primary key of the t country
	 * @return the t languages associated with the t country
	 */
	public java.util.List<TLanguage> getTCountryTLanguages(long pk);

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t country
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @return the range of t languages associated with the t country
	 */
	public java.util.List<TLanguage> getTCountryTLanguages(
		long pk, int start, int end);

	/**
	 * Returns all the t language associated with the t country.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param pk the primary key of the t country
	 * @param start the lower bound of the range of t countries
	 * @param end the upper bound of the range of t countries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of t languages associated with the t country
	 */
	public java.util.List<TLanguage> getTCountryTLanguages(
		long pk, int start, int end,
		OrderByComparator<TLanguage> orderByComparator);

	/**
	 * Returns the number of t countries associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @return the number of t countries associated with the t language
	 */
	public int getTCountriesSize(long pk);

	/**
	 * Returns <code>true</code> if the t country is associated with the t language.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 * @return <code>true</code> if the t country is associated with the t language; <code>false</code> otherwise
	 */
	public boolean containsTCountry(long pk, long tCountryPK);

	/**
	 * Returns <code>true</code> if the t language has any t countries associated with it.
	 *
	 * @param pk the primary key of the t language to check for associations with t countries
	 * @return <code>true</code> if the t language has any t countries associated with it; <code>false</code> otherwise
	 */
	public boolean containsTCountries(long pk);

	/**
	 * Adds an association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 */
	public void addTCountry(long pk, long tCountryPK);

	/**
	 * Adds an association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountry the t country
	 */
	public void addTCountry(
		long pk, com.aimprosoft.poc.sb.model.TCountry tCountry);

	/**
	 * Adds an association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries
	 */
	public void addTCountries(long pk, long[] tCountryPKs);

	/**
	 * Adds an association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries
	 */
	public void addTCountries(
		long pk,
		java.util.List<com.aimprosoft.poc.sb.model.TCountry> tCountries);

	/**
	 * Clears all associations between the t language and its t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language to clear the associated t countries from
	 */
	public void clearTCountries(long pk);

	/**
	 * Removes the association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPK the primary key of the t country
	 */
	public void removeTCountry(long pk, long tCountryPK);

	/**
	 * Removes the association between the t language and the t country. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountry the t country
	 */
	public void removeTCountry(
		long pk, com.aimprosoft.poc.sb.model.TCountry tCountry);

	/**
	 * Removes the association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries
	 */
	public void removeTCountries(long pk, long[] tCountryPKs);

	/**
	 * Removes the association between the t language and the t countries. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries
	 */
	public void removeTCountries(
		long pk,
		java.util.List<com.aimprosoft.poc.sb.model.TCountry> tCountries);

	/**
	 * Sets the t countries associated with the t language, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountryPKs the primary keys of the t countries to be associated with the t language
	 */
	public void setTCountries(long pk, long[] tCountryPKs);

	/**
	 * Sets the t countries associated with the t language, removing and adding associations as necessary. Also notifies the appropriate model listeners and clears the mapping table finder cache.
	 *
	 * @param pk the primary key of the t language
	 * @param tCountries the t countries to be associated with the t language
	 */
	public void setTCountries(
		long pk,
		java.util.List<com.aimprosoft.poc.sb.model.TCountry> tCountries);

}