/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.aimprosoft.poc.sb.service;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for TLanguage. This utility wraps
 * <code>com.aimprosoft.poc.sb.service.impl.TLanguageLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see TLanguageLocalService
 * @generated
 */
@ProviderType
public class TLanguageLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.aimprosoft.poc.sb.service.impl.TLanguageLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static void addTCountryTLanguage(long countryId, long languageId) {
		getService().addTCountryTLanguage(countryId, languageId);
	}

	public static void addTCountryTLanguage(
		long countryId, com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		getService().addTCountryTLanguage(countryId, tLanguage);
	}

	public static void addTCountryTLanguages(
		long countryId,
		java.util.List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages) {

		getService().addTCountryTLanguages(countryId, tLanguages);
	}

	public static void addTCountryTLanguages(
		long countryId, long[] languageIds) {

		getService().addTCountryTLanguages(countryId, languageIds);
	}

	/**
	 * Adds the t language to the database. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was added
	 */
	public static com.aimprosoft.poc.sb.model.TLanguage addTLanguage(
		com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		return getService().addTLanguage(tLanguage);
	}

	public static void clearTCountryTLanguages(long countryId) {
		getService().clearTCountryTLanguages(countryId);
	}

	/**
	 * Creates a new t language with the primary key. Does not add the t language to the database.
	 *
	 * @param languageId the primary key for the new t language
	 * @return the new t language
	 */
	public static com.aimprosoft.poc.sb.model.TLanguage createTLanguage(
		long languageId) {

		return getService().createTLanguage(languageId);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static void deleteTCountryTLanguage(
		long countryId, long languageId) {

		getService().deleteTCountryTLanguage(countryId, languageId);
	}

	public static void deleteTCountryTLanguage(
		long countryId, com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		getService().deleteTCountryTLanguage(countryId, tLanguage);
	}

	public static void deleteTCountryTLanguages(
		long countryId,
		java.util.List<com.aimprosoft.poc.sb.model.TLanguage> tLanguages) {

		getService().deleteTCountryTLanguages(countryId, tLanguages);
	}

	public static void deleteTCountryTLanguages(
		long countryId, long[] languageIds) {

		getService().deleteTCountryTLanguages(countryId, languageIds);
	}

	/**
	 * Deletes the t language with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language that was removed
	 * @throws PortalException if a t language with the primary key could not be found
	 */
	public static com.aimprosoft.poc.sb.model.TLanguage deleteTLanguage(
			long languageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteTLanguage(languageId);
	}

	/**
	 * Deletes the t language from the database. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was removed
	 */
	public static com.aimprosoft.poc.sb.model.TLanguage deleteTLanguage(
		com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		return getService().deleteTLanguage(tLanguage);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.aimprosoft.poc.sb.model.TLanguage fetchTLanguage(
		long languageId) {

		return getService().fetchTLanguage(languageId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static com.aimprosoft.poc.sb.model.TLanguage getLanguageByCode(
		String languageCode) {

		return getService().getLanguageByCode(languageCode);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the countryIds of the t countries associated with the t language.
	 *
	 * @param languageId the languageId of the t language
	 * @return long[] the countryIds of t countries associated with the t language
	 */
	public static long[] getTCountryPrimaryKeys(long languageId) {
		return getService().getTCountryPrimaryKeys(languageId);
	}

	public static java.util.List<com.aimprosoft.poc.sb.model.TLanguage>
		getTCountryTLanguages(long countryId) {

		return getService().getTCountryTLanguages(countryId);
	}

	public static java.util.List<com.aimprosoft.poc.sb.model.TLanguage>
		getTCountryTLanguages(long countryId, int start, int end) {

		return getService().getTCountryTLanguages(countryId, start, end);
	}

	public static java.util.List<com.aimprosoft.poc.sb.model.TLanguage>
		getTCountryTLanguages(
			long countryId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.aimprosoft.poc.sb.model.TLanguage> orderByComparator) {

		return getService().getTCountryTLanguages(
			countryId, start, end, orderByComparator);
	}

	public static int getTCountryTLanguagesCount(long countryId) {
		return getService().getTCountryTLanguagesCount(countryId);
	}

	/**
	 * Returns the t language with the primary key.
	 *
	 * @param languageId the primary key of the t language
	 * @return the t language
	 * @throws PortalException if a t language with the primary key could not be found
	 */
	public static com.aimprosoft.poc.sb.model.TLanguage getTLanguage(
			long languageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getTLanguage(languageId);
	}

	/**
	 * Returns a range of all the t languages.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.aimprosoft.poc.sb.model.impl.TLanguageModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of t languages
	 * @param end the upper bound of the range of t languages (not inclusive)
	 * @return the range of t languages
	 */
	public static java.util.List<com.aimprosoft.poc.sb.model.TLanguage>
		getTLanguages(int start, int end) {

		return getService().getTLanguages(start, end);
	}

	/**
	 * Returns the number of t languages.
	 *
	 * @return the number of t languages
	 */
	public static int getTLanguagesCount() {
		return getService().getTLanguagesCount();
	}

	public static boolean hasTCountryTLanguage(
		long countryId, long languageId) {

		return getService().hasTCountryTLanguage(countryId, languageId);
	}

	public static boolean hasTCountryTLanguages(long countryId) {
		return getService().hasTCountryTLanguages(countryId);
	}

	public static void setTCountryTLanguages(
		long countryId, long[] languageIds) {

		getService().setTCountryTLanguages(countryId, languageIds);
	}

	/**
	 * Updates the t language in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param tLanguage the t language
	 * @return the t language that was updated
	 */
	public static com.aimprosoft.poc.sb.model.TLanguage updateTLanguage(
		com.aimprosoft.poc.sb.model.TLanguage tLanguage) {

		return getService().updateTLanguage(tLanguage);
	}

	public static TLanguageLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TLanguageLocalService, TLanguageLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(TLanguageLocalService.class);

		ServiceTracker<TLanguageLocalService, TLanguageLocalService>
			serviceTracker =
				new ServiceTracker
					<TLanguageLocalService, TLanguageLocalService>(
						bundle.getBundleContext(), TLanguageLocalService.class,
						null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}